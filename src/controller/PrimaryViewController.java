package controller;

import application.Main;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import model.DataModel;
import model.PatientLite;

/**
 * This controller synchronizes selections in the primary list view with the
 * currently selected client in the data model.
 * 
 * @author David Jarrett
 * @version 09/16/2018
 */
public class PrimaryViewController extends MVPController {

	@FXML
	private ListView<PatientLite> lsvClients;
	@FXML
	private Label firstNameLabel;
	@FXML
	private Label lastNameLabel;
	@FXML
	private Label clientIDLabel;

	@Override
	public void initialize(Main mainWindow, DataModel dataModel) {
		super.initialize(mainWindow, dataModel);
		this.setupCurrentPersonBinding();
		this.setupCellFactory();
	}

	private void setupCellFactory() {
		lsvClients.setCellFactory(lv -> new ListCell<PatientLite>() {
			@Override
			public void updateItem(PatientLite person, boolean empty) {
				super.updateItem(person, empty);
				if (empty) {
					setText(null);
				} else {
					setText(person.getFirstName() + " " + person.getLastName());
				}
			}
		});
	}

	private void setupCurrentPersonBinding() {
		this.lsvClients.setItems(this.getDataModel().getPatientList());
		lsvClients.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> this.getDataModel().setCurrentPerson(newSelection));

		this.getDataModel().getCurrentPersonProperty().addListener((obs, oldPerson, newPerson) -> {
			if (newPerson == null) {
				lsvClients.getSelectionModel().clearSelection();
			} else {
				lsvClients.getSelectionModel().select(newPerson);
			}
		});
	}
}
