package controller;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import application.Main;
import dal.MySqlPatientDAL;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.AddPatientManager;
import model.DataModel;
import model.Patient;
import model.PatientLite;
import model.State;
import util.ValidationTools;

/**
 * Controller for adding and updating patient records
 * 
 * @author Ras Fincher & David Jarrett
 * @version Fall 2018
 */
public class PatientMaintenanceWindowController extends MVPController {

	private static final int DUPLICATE_ENTRY_ERROR_CODE = 1062;
	private BooleanBinding canSubmit;

	@FXML
	private TextField firstNameTextField;
	@FXML
	private Label firstNameLabel;
	@FXML
	private TextField lastNameTextField;
	@FXML
	private Label lastNameLabel;
	@FXML
	private Label genderLabel;
	@FXML
	private ComboBox<String> genderComboBox;
	@FXML
	private Label dobLabel;
	@FXML
	private DatePicker dobDatePicker;
	@FXML
	private Label ssnLabel;
	@FXML
	private TextField ssnTextField;
	@FXML
	private Label phoneLabel;
	@FXML
	private TextField phoneTextField;
	@FXML
	private Label streetLabel;
	@FXML
	private TextField streetTextField;
	@FXML
	private Label cityLabel;
	@FXML
	private TextField cityTextField;
	@FXML
	private Label stateLable;
	@FXML
	private ComboBox<State> stateComboBox;
	@FXML
	private Label zipLabel;
	@FXML
	private TextField zipTextField;
	@FXML
	private Button addPatientButton;
	@FXML
	private Button cancelButton;
	private boolean isEditing;

	@FXML
	private void onAddPatientButtonClicked(ActionEvent event) {
		AddPatientManager addUserManager = new AddPatientManager(this);

		try {
			addUserManager.addPatientToDataModel(this.isEditing);
			this.closeWindow(this.addPatientButton);
		} catch (SQLException e) {
			if (e.getErrorCode() == DUPLICATE_ENTRY_ERROR_CODE) {
				handleDuplicateEntryError();
			} else {
				e.printStackTrace();
			}
		}
	}

	private void handleDuplicateEntryError() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Duplicate Entry Error");
		alert.setHeaderText("Record Already Exists");
		alert.setContentText("A record already exists with the given social security number.");
		alert.showAndWait();
		this.ssnTextField.requestFocus();
	}

	@FXML
	void onCancelButtonClicked(ActionEvent event) {
		this.closeWindow(this.cancelButton);
	}

	/**
	 * Initializes the MVPController with the main window and data model.
	 * 
	 * @preconditions: mainWindow != null && dataModel != null
	 * @postconditions: MPVController will be initialized.
	 */
	@Override
	public void initialize(Main mainWindow, DataModel dataModel) {
		super.initialize(mainWindow, dataModel);
		this.setData();
		this.populateGenderComboBox();
		this.configureDatePicker();
		this.setupValidationBinding();
	}

	/**
	 * Method that updates an existing patient record
	 * 
	 * @param editMode denotes if the patient is being edited or not
	 */
	public void editPatient(boolean editMode) {
		this.isEditing = true;
		this.addPatientButton.setText("Submit");

		PatientLite patientLite = this.getDataModel().getCurrentPerson();
		Patient currentPatient = new MySqlPatientDAL().getDataForPatient(patientLite.getPatientID());
		this.firstNameTextField.setText(currentPatient.getFirstName());
		this.lastNameTextField.setText(currentPatient.getLastName());
		if (currentPatient.getGender().equals("Male")) {
			this.genderComboBox.getSelectionModel().select(0);
		} else {
			this.genderComboBox.getSelectionModel().select(1);
		}
		this.dobDatePicker.setValue(currentPatient.getDateOfBirth());
		this.ssnTextField.setText(currentPatient.getSSN());
		this.phoneTextField.setText(currentPatient.getPhone());
		this.streetTextField.setText(currentPatient.getAddress().getStreet());
		this.cityTextField.setText(currentPatient.getAddress().getCity());
		this.stateComboBox.getSelectionModel().select(currentPatient.getAddress().getState());
		this.zipTextField.setText(currentPatient.getAddress().getZip());

		this.firstNameTextField.setDisable(true);
		this.lastNameTextField.setDisable(true);
		this.ssnTextField.setDisable(true);
	}

	/**
	 * Returns the value stored in firstNameTextField
	 * 
	 * @return the value stored in firstNameTextField
	 */
	public String getFirstNameText() {
		return this.firstNameTextField.getText();
	}

	/**
	 * Returns the value stored in lastNameTextField
	 * 
	 * @return the value stored in lastNameTextField
	 */
	public String getLastNameText() {
		return this.lastNameTextField.getText();
	}

	/**
	 * Returns the value stored in genderComboBox
	 * 
	 * @return the value stored in genderComboBox
	 */
	public String getGender() {
		return this.genderComboBox.getValue();
	}

	/**
	 * Returns the LocalDate object stored in dobDatePicker
	 * 
	 * @return the LocalDate object stored in dobDatePicker
	 */
	public LocalDate getDOB() {
		return this.dobDatePicker.getValue();
	}

	/**
	 * Returns the value stored in ssnTextField
	 * 
	 * @return the value stored in ssnTextField
	 */
	public String getSSN() {
		return this.ssnTextField.getText();
	}

	/**
	 * Returns the String store in phoneTextField
	 * 
	 * @return the String store in phoneTextField
	 */
	public String getPhone() {
		return this.phoneTextField.getText();
	}

	/**
	 * Return the String stored in streetTextField
	 * 
	 * @return the String stored in streetTextField
	 */
	public String getStreet() {
		return this.streetTextField.getText();
	}

	/**
	 * Return the String stored in cityTextField
	 * 
	 * @return the String stored in cityTextField
	 */
	public String getCity() {
		return this.cityTextField.getText();
	}

	/**
	 * Returns the selected State from stateComboBox
	 * 
	 * @return the selected State from stateComboBox
	 */
	public State getState() {
		return this.stateComboBox.getValue();
	}

	/**
	 * Returns the String stored in zipTextField
	 * 
	 * @return the String stored in zipTextField
	 */
	public String getZip() {
		return this.zipTextField.getText();
	}

	/**
	 * Returns a handle to TextField phoneTextField
	 * 
	 * @return a handle to the TextField phoneTextField
	 */
	public TextField getPhoneTextField() {
		return this.phoneTextField;
	}

	/**
	 * Returns a handle to TextField phoneTextField
	 * 
	 * @return a handle to the TextField phoneTextField
	 */
	public TextField getZipTextField() {
		return this.zipTextField;
	}

	/**
	 * Returns a handle to the DatePicker dobDatePicker
	 * 
	 * @return a handle to the dobDatePicker
	 */
	public DatePicker getDOBPicker() {
		return this.dobDatePicker;
	}

	private void configureDatePicker() {
		this.dobDatePicker.setConverter(new StringConverter<LocalDate>() {
			String pattern = "MM-dd-yyyy";
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

			{
				dobDatePicker.setPromptText(pattern.toLowerCase());
			}

			@Override
			public LocalDate fromString(String string) {
				if (string != null && !string.isEmpty()) {
					return LocalDate.parse(string, dateFormatter);
				} else {
					return null;
				}
			}

			@Override
			public String toString(LocalDate date) {
				if (date != null) {
					return dateFormatter.format(date);
				} else {
					return "";
				}
			}

		});
		this.addListenerToDatePicker();
	}

	private void addListenerToDatePicker() {
		this.dobDatePicker.focusedProperty().addListener((ov, oldValue, newValue) -> {
			if (!newValue) {
				try {
					this.dobDatePicker
							.setValue(dobDatePicker.getConverter().fromString(dobDatePicker.getEditor().getText()));
				} catch (DateTimeParseException e) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Input Error");
					alert.setHeaderText("Invalid Date Entered");
					alert.setContentText(dobDatePicker.getEditor().getText() + " is not a valid date. "
							+ "Please enter a date in the formate MM-DD-YYYY");
					alert.showAndWait();
					this.dobDatePicker.requestFocus();
				}

			}
		});
	}

	private void setData() {
		this.stateComboBox.getItems().clear();
		this.stateComboBox.getItems().setAll(State.values());
	}

	private void setupValidationBinding() {
		this.canSubmit = new BooleanBinding() {
			{
				super.bind(firstNameTextField.textProperty(), lastNameTextField.textProperty(),
						dobDatePicker.valueProperty(), phoneTextField.textProperty(), streetTextField.textProperty(),
						cityTextField.textProperty(), stateComboBox.valueProperty(), zipTextField.textProperty(),
						genderComboBox.valueProperty(), ssnTextField.textProperty());
			}

			@Override
			protected boolean computeValue() {
				updateFirstNameFieldColor();
				updateLastNameFieldColor();
				updateGenderFieldColor();
				updateDOBFieldColor();
				updateAddressFieldColor();
				updateCityFieldColor();
				updateStateFieldColor();
				updateZipFieldColor();
				updateSSNFieldColor();
				updatePhoneFieldColor();
				return firstNameTextField.getText().isEmpty() || lastNameTextField.getText().isEmpty()
						|| dobDatePicker.getValue() == null || phoneTextField.getText().isEmpty()
						|| streetTextField.getText().isEmpty() || cityTextField.getText().isEmpty()
						|| stateComboBox.getValue() == null || zipTextField.getText().isEmpty()
						|| genderComboBox.getValue() == null
						|| !ValidationTools.validatePhoneNumber(phoneTextField.getText())
						|| !ValidationTools.validateZipCode(getZip()) || !ValidationTools.validateSSN(getSSN());
			}

		};
		this.addPatientButton.disableProperty().bind(this.canSubmit);
	}

	private void updateZipFieldColor() {
		if (ValidationTools.validateZipCode(getZip())) {
			this.zipLabel.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.zipLabel.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateSSNFieldColor() {
		if (this.isEditing) {
			this.ssnLabel.textFillProperty().set(Paint.valueOf("BLACK"));
		} else {
			if (ValidationTools.validateSSN(getSSN())) {
				this.ssnLabel.textFillProperty().set(Paint.valueOf("GREEN"));
			} else {
				this.ssnLabel.textFillProperty().set(Paint.valueOf("RED"));
			}
		}
	}

	private void updatePhoneFieldColor() {
		if (ValidationTools.validatePhoneNumber(getPhone())) {
			this.phoneLabel.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.phoneLabel.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateFirstNameFieldColor() {
		if (this.isEditing) {
			this.firstNameLabel.textFillProperty().set(Paint.valueOf("BLACK"));
		} else {
			if (!getFirstNameText().equals("")) {
				this.firstNameLabel.textFillProperty().set(Paint.valueOf("GREEN"));
			} else {
				this.firstNameLabel.textFillProperty().set(Paint.valueOf("RED"));
			}
		}
	}

	private void updateLastNameFieldColor() {
		if (this.isEditing) {
			this.lastNameLabel.textFillProperty().set(Paint.valueOf("BLACK"));
		} else {
			if (!getLastNameText().equals("")) {
				this.lastNameLabel.textFillProperty().set(Paint.valueOf("GREEN"));
			} else {
				this.lastNameLabel.textFillProperty().set(Paint.valueOf("RED"));
			}
		}
	}

	private void updateGenderFieldColor() {
		if (getGender() != null) {
			this.genderLabel.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.genderLabel.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateDOBFieldColor() {
		if (getDOB() != null) {
			this.dobLabel.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.dobLabel.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateAddressFieldColor() {
		if (!this.getStreet().equals("")) {
			this.streetLabel.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.streetLabel.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateCityFieldColor() {
		if (!this.getCity().equals("")) {
			this.cityLabel.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.cityLabel.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateStateFieldColor() {
		if (this.stateComboBox.getValue() != null) {
			this.stateLable.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.stateLable.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void populateGenderComboBox() {
		ObservableList<String> genders = FXCollections.observableArrayList("Male", "Female");

		this.genderComboBox.setItems(genders);
	}

	private void closeWindow(Button button) {
		Stage stage = (Stage) button.getScene().getWindow();
		stage.close();
	}

}
