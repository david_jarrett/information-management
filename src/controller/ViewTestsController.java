package controller;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import application.Main;
import dal.MySqlTestDAL;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import model.DataModel;
import model.Test;

public class ViewTestsController extends MVPController {

	@FXML
    private ListView<Test> lstViewTests = new ListView<>();
    @FXML
    private Button btnClose;
    @FXML
    private TextField txtFldResults;
    @FXML
    private TextField txtFldDate;
    
    /**
     * Initializes the MVPController with the main window and data model.
     * @preconditions: mainWindow != null && dataModel != null
     * @postconditions: MPVController will be initialized.
     */
    @Override
	public void initialize(Main mainWindow, DataModel dataModel) {
		super.initialize(mainWindow, dataModel);
		var testDAL = new MySqlTestDAL();
		var tests = testDAL.getTestsFor(this.getDataModel().getSelectedAppointmentID());
		this.lstViewTests.setItems(FXCollections.observableArrayList(tests));
		
		this.lstViewTests.getSelectionModel().selectedItemProperty()
		.addListener((obs, oldSelection, newSelection) -> {
			if (oldSelection != null) {
				this.saveRecord(oldSelection);
			}
			int visitID = this.lstViewTests.getSelectionModel().getSelectedItem().getVisitID();
			int testID = this.lstViewTests.getSelectionModel().getSelectedItem().getTestID();
			this.txtFldResults.setText(testDAL.getTestResultsFor(testID, visitID));
			this.txtFldDate.setText(testDAL.getTestResultDateFor(testID, visitID));
		});
	}
    
	@FXML
	private void onBtnCloseClicked(ActionEvent event) {
		if (this.saveRecord()) {
			this.btnClose.getScene().getWindow().hide();
		}
	}
	
	private boolean saveRecord() {
		var currentTest = this.lstViewTests.getSelectionModel().getSelectedItem();
		return this.saveRecord(currentTest);
	}

	private boolean saveRecord(Test currentTest) {
		if (currentTest == null) {
			return false;
		}
		
		try {
			String dateText = this.txtFldDate.getText();
			if (dateText.isEmpty() && !this.txtFldResults.getText().isEmpty()) {
				this.showDateErrorMessage("You must enter a date for the test results.");
				return false;
			}
			dateText = this.convertDate(dateText);
			
			var testDAL = new MySqlTestDAL();
			currentTest.setTestResults(this.txtFldResults.getText());
			currentTest.setTestDate(Date.valueOf(dateText));
			testDAL.updateResultsFor(currentTest);
		} catch (IllegalArgumentException e) {
			this.showDateErrorMessage("Please enter date with the format: MM/DD/YYYY");
		} catch (NullPointerException e) {
			//pass
		}
		return true;
	}
	
	private void showDateErrorMessage(String message) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Date Error");
		alert.setContentText(message);
		alert.showAndWait();
	}

	private String convertDate(String date) {
		if (date.isEmpty()) {
			return date;
		}
		int yearIndex = date.lastIndexOf('/');
		if (yearIndex == -1) {
			this.showDateErrorMessage("Invalid Date Format: Please use MM/DD/YYYY");
			return "";
		}
		String newDate =  date.substring(yearIndex + 1, date.length()) + "-" + date.substring(0, yearIndex);
		return newDate.replace('/', '-');
	}
	
	public void lockWindow() {
		this.txtFldResults.setDisable(true);
		this.txtFldDate.setDisable(true);
		this.btnClose.setText("Close");
	}
	
	@FXML
	private void setDateToToday(ActionEvent event) {
		LocalDate today = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String formattedString = today.format(formatter);
		this.txtFldDate.setText(formattedString);
	}
	
}
