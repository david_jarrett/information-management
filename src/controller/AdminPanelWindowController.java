package controller;

import java.sql.SQLException;

import application.Main;
import dal.MySqlSystemUserDAL;
import dal.SystemUserDAL;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.DataModel;

/**
 * Controller for the admin panel.
 * @author Ras Fincher
 * @version Fall 2018
 */
public class AdminPanelWindowController extends MVPController {
	@FXML
    private TextArea txtArea_command;

    @FXML
    private Button btn_submit;

    @FXML
    private TextArea txtArea_results;

    @FXML
    private Button btn_close;
    
    @FXML
    private Button btn_clear;
    
    /**
     * Initializes the MVPController with the main window and data model.
     * @preconditions: mainWindow != null && dataModel != null
     * @postconditions: MPVController will be initialized.
     */
    @Override
	public void initialize(Main mainWindow, DataModel dataModel) {
		super.initialize(mainWindow, dataModel);
		this.btn_submit.disableProperty().bind(Bindings.isEmpty(this.txtArea_command.textProperty()));
		this.btn_clear.disableProperty().bind(Bindings.isEmpty(this.txtArea_command.textProperty()));
	}
    
    @FXML
    private void submitButtonClicked(ActionEvent event) {
    	SystemUserDAL dal = new MySqlSystemUserDAL();
    	
    	String query = this.txtArea_command.getText().replaceAll("[\r\n\t]+", "");
    	String result = "";
    	
    	try {
			result = dal.queryResult(query);
		} catch (SQLException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("SQL Error");
			alert.setHeaderText("An SQL Error occurred");
			alert.setContentText("The error code is: " + e.getErrorCode());
			alert.showAndWait();
		}
    	
    	this.txtArea_results.setText(result);
    }
    
    @FXML
    private void closeButtonClicked(ActionEvent event) {
    	this.closeWindow(this.btn_close);
    }
    
    @FXML
    private void onClearClicked(ActionEvent event) {
    	this.txtArea_command.setText("");
    	this.txtArea_results.setText("");
    }
    
    private void closeWindow(Button button) {
    	Stage stage = (Stage) button.getScene().getWindow();
    	stage.close();
    }
}
