package controller;

import application.Main;
import dal.MySqlPatientDAL;
import dal.PatientDAL;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import model.DataModel;
import model.Patient;

/**
 * This controller synchronizes the display of patient information for the
 * currently selected patient in the data model.
 * @author David Jarrett
 * @version 10/13/2018
 */
public class PrimaryViewPatientDisplayController extends MVPController {

	@FXML
    private Label firstNameLabel;
    @FXML
    private Label lastNameLabel;
    @FXML
    private Label patientIDLabel;
    @FXML
    private Label dateOfBirthLabel;
    @FXML
    private Label streetLabel;
    @FXML
    private Label cityLabel;
    @FXML
    private Label stateLabel;
    @FXML
    private Label phoneLabel;
    @FXML
    private Label genderLabel;
    @FXML
    private Label ssnLabel;
    
    private PatientDAL patientDAL;
    
    /**
     * Initializes the controller by setting up binding.
     * @preconditions (mainApplication && dataModel) != null
     * @postconditions Selected person in list view will be bound to selected person in data model.
     */
    @Override
    public void initialize(Main mainApplication, DataModel dataModel) {
    	super.initialize(mainApplication, dataModel);
    	this.patientDAL = new MySqlPatientDAL();
    	
    	this.getDataModel().getCurrentPersonProperty().addListener((obs, oldPerson, newPerson) -> {
    		if (oldPerson != null) {
                unbindData(patientDAL.getDataForPatient(oldPerson.getPatientID()));
            }
            if (newPerson == null) {
                //clearTextFields();
            } else {
                bindData(patientDAL.getDataForPatient(newPerson.getPatientID()));
            }
        });
    }
    

	private void bindData(Patient newPerson) {
		newPerson.setPatientIDProperty(this.patientDAL.getPatientLite(newPerson).getPatientID());
		
		this.firstNameLabel.textProperty().bindBidirectional(newPerson.getFirstNameProperty());
		this.lastNameLabel.textProperty().bindBidirectional(newPerson.getLastNameProperty());
		this.patientIDLabel.textProperty().bindBidirectional(newPerson.getPatientIDProperty());
		this.dateOfBirthLabel.textProperty().bind(newPerson.getDateOfBirthProperty().asString());
		this.streetLabel.textProperty().bindBidirectional(newPerson.getAddressProperty().get().getStreetProperty());
		this.cityLabel.textProperty().bindBidirectional(newPerson.getAddressProperty().get().getCityProperty());
		this.stateLabel.textProperty().bind(newPerson.getAddressProperty().get().getStateProperty().asString());
		this.phoneLabel.textProperty().bindBidirectional(newPerson.getPhoneProperty());
		this.genderLabel.textProperty().bindBidirectional(newPerson.getGenderProperty());
		this.ssnLabel.textProperty().bindBidirectional(newPerson.getSSNProperty());
	}

	private void unbindData(Patient oldPerson) {
		this.firstNameLabel.textProperty().unbindBidirectional(oldPerson.getFirstNameProperty());
		this.lastNameLabel.textProperty().unbindBidirectional(oldPerson.getLastNameProperty());
		this.patientIDLabel.textProperty().unbindBidirectional(oldPerson.getPatientIDProperty());
		this.dateOfBirthLabel.textProperty().unbindBidirectional(oldPerson.getDateOfBirthProperty());
		this.streetLabel.textProperty().unbindBidirectional(oldPerson.getAddressProperty().get().getStreetProperty());
		this.cityLabel.textProperty().unbindBidirectional(oldPerson.getAddressProperty().get().getCityProperty());
		this.stateLabel.textProperty().unbindBidirectional(oldPerson.getAddressProperty().get().getStateProperty());
		this.phoneLabel.textProperty().unbindBidirectional(oldPerson.getPhoneProperty());
		this.genderLabel.textProperty().unbindBidirectional(oldPerson.getGenderProperty());
		this.ssnLabel.textProperty().unbindBidirectional(oldPerson.getSSNProperty());
	}
	
}
