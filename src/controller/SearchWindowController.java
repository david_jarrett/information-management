package controller;

import java.util.List;

import application.Main;
import dal.MySqlPatientDAL;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.DataModel;
import model.PatientLite;

public class SearchWindowController extends MVPController {

	@FXML
	private TextField txtBoxFirstName;
	@FXML
	private TextField txtBoxLastName;
	@FXML
	private TextField txtBoxDob;
	@FXML
	private Button btnSearch;
	@FXML
	private Button btn_selectPatient;
	@FXML
	private ListView<PatientLite> listView_results;

	/**
     * Initializes the MVPController with the main window and data model.
     * @preconditions: mainWindow != null && dataModel != null
     * @postconditions: MPVController will be initialized.
     */
	@Override
	public void initialize(Main mainWindow, DataModel dataModel) {
		super.initialize(mainWindow, dataModel);
		this.btn_selectPatient.setDisable(true);
	}

	@FXML
	private void doSearch(ActionEvent event) {
		if (this.txtBoxFirstName.getText().isEmpty() && this.txtBoxLastName.getText().isEmpty()) {
			this.doDobSearch();
		} else {
			this.doNameSearch();
		}
	}

	@FXML
	private void onSelectPatientClicked(ActionEvent event) {
		PatientLite currentPatient = this.listView_results.getSelectionModel().getSelectedItem();
		this.getDataModel().setCurrentPerson(currentPatient);

		this.closeWindow(btn_selectPatient);

	}

	private void doDobSearch() {
		if (this.txtBoxDob.getText().isEmpty()) {
			this.showError("You must search by either firstname/lastname, dob, or both.");
			return;
		}

		if (!this.validateDate(this.txtBoxDob.getText())) {
			this.showError("Date must be in format: YYYY-MM-DD");
			return;
		}

		String dob = this.txtBoxDob.getText();

		List<PatientLite> patients = new MySqlPatientDAL().getPatients(dob);

		this.listView_results.setItems(FXCollections.observableArrayList(patients));
		this.btn_selectPatient.disableProperty()
				.bind(Bindings.isEmpty(this.listView_results.getSelectionModel().getSelectedItems()));
	}

	private void doNameSearch() {
		if (this.txtBoxFirstName.getText().isEmpty() || this.txtBoxLastName.getText().isEmpty()) {
			this.showError("Name search requires both first and last names.");
			return;
		}
		if (!this.txtBoxDob.getText().isEmpty()) {
			this.includeDob();
			return;
		}

		String firstName = this.txtBoxFirstName.getText();
		String lastName = this.txtBoxLastName.getText();

		List<PatientLite> patients = new MySqlPatientDAL().getPatients(firstName, lastName);

		this.listView_results.setItems(FXCollections.observableArrayList(patients));
		this.btn_selectPatient.disableProperty()
				.bind(Bindings.isEmpty(this.listView_results.getSelectionModel().getSelectedItems()));
	}

	private void includeDob() {
		String firstName = this.txtBoxFirstName.getText();
		String lastName = this.txtBoxLastName.getText();

		if (!this.validateDate(this.txtBoxDob.getText())) {
			this.showError("Date must be in format: YYYY-MM-DD");
			return;
		}
		String dob = this.txtBoxDob.getText();

		List<PatientLite> patients = new MySqlPatientDAL().getPatients(firstName, lastName, dob);

		this.listView_results.setItems(FXCollections.observableArrayList(patients));
		this.btn_selectPatient.disableProperty()
				.bind(Bindings.isEmpty(this.listView_results.getSelectionModel().getSelectedItems()));
	}

	private boolean validateDate(String dateStr) {
		return dateStr.matches("\\d{4}[-\\s]\\d{2}[-\\s]\\d{2}");
	}

	private void showError(String message) {
		var alert = new Alert(Alert.AlertType.ERROR);
		alert.setContentText(message);
		alert.setHeaderText("Error");
		alert.showAndWait();
	}

	private void closeWindow(Button button) {
		Stage stage = (Stage) button.getScene().getWindow();
		stage.close();
	}

}
