package controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import model.LoginManager;

/**
 * Controller for the login window. Upon successful login, it loads the primary window.
 * @author David Jarrett
 * @version 09/15/2018
 */
public class LoginWindowController extends MVPController {
	
	private static final String PRIMARY_VIEW_MENU_BAR_FXML_PATH = "../view/PrimaryViewMenuBar.fxml";

	private static final String PRIMARY_VIEW_CLIENT_DISPLAY_FXML_PATH = "../view/PrimaryViewClientDisplay.fxml";

	private static final String PRIMARY_VIEW_INFORMATION_BAR_PATH = "../view/PrimaryViewInformationBar.fxml";
	
	@FXML
    private TextField txtUserName;

	@FXML
    private PasswordField txtPassword;
	
    @FXML
    void onLoginClicked(ActionEvent event) {
    	loginRequested();
    }
    
    @FXML
    public void onEnter(ActionEvent event){
       loginRequested();
    }

	private void loginRequested() {
		if (LoginManager.getInstance().logUserIn(this.txtUserName.getText(), this.txtPassword.getText())) {
    		this.loadNewScene();
    		this.txtUserName.setText("");
    		this.txtPassword.setText("");
    	} else {
    		Alert invalidPasswordAlert = new Alert(AlertType.ERROR, "Invalid username/password.");
    		invalidPasswordAlert.showAndWait();
    	}
	}
    
    private void loadNewScene() {
		try {
			BorderPane newRoot = createNewRoot();
			resetScene(newRoot);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private BorderPane createNewRoot() throws IOException {
		BorderPane newRoot = new BorderPane();
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource(PRIMARY_VIEW_CLIENT_DISPLAY_FXML_PATH));
		newRoot.setCenter(loader.load());
		MVPController controller = loader.getController();
		controller.initialize(this.getMainApplication(), this.getDataModel());
		
		loader = new FXMLLoader(getClass().getResource(PRIMARY_VIEW_MENU_BAR_FXML_PATH));
		newRoot.setTop(loader.load());
		controller = loader.getController();
		controller.initialize(this.getMainApplication(), this.getDataModel());
		
		loader = new FXMLLoader(getClass().getResource(PRIMARY_VIEW_INFORMATION_BAR_PATH));
		newRoot.setLeft(loader.load());
		controller = loader.getController();
		controller.initialize(this.getMainApplication(), this.getDataModel());
		
		return newRoot;
	}
	
	private void resetScene(BorderPane newRoot) {
		this.getMainApplication().getPrimaryStage().hide();
		this.getMainApplication().getPrimaryStage().setScene(new Scene(newRoot, 850, 550));
		this.getMainApplication().getPrimaryStage().setTitle("Healthcare System");
		this.getMainApplication().getPrimaryStage().setResizable(true);
		this.getMainApplication().getPrimaryStage().show();
	}
	
}
