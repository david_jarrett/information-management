package controller;

import application.Main;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import model.DataModel;
import model.PatientLite;

/**
 * This controller keeps the primary list-view in sync with the data model. 
 * @author David Jarrett
 * @version 09/15/2018
 */
public class PrimaryListViewController extends MVPController {

	@FXML
	private ListView<PatientLite> lsvPatients;

	@FXML
	private Label firstNameLabel;

	@FXML
	private Label lastNameLabel;

	@FXML
	private Label patientIDLabel;
	
	/**
	 * Initializes the binding for the list-view and sets the cell-factory for how patients are displayed.
	 * @preconditions (mainWindow && dataModel) != null
	 * @postconditions Bindings will be initialized.
	 */
	@Override
	public void initialize(Main mainWindow, DataModel dataModel) {
		super.initialize(mainWindow, dataModel);
		this.lsvPatients.setItems(this.getDataModel().getPatientList());
		this.setupCurrentPersonBinding();
		this.setupCellFactory();
	}

	private void setupCellFactory() {
		lsvPatients.setCellFactory(lv -> new ListCell<PatientLite>() {
			@Override
			public void updateItem(PatientLite person, boolean empty) {
				super.updateItem(person, empty);
				if (empty) {
					setText(null);
				} else {
					setText(person.getLastName() + ", " + person.getFirstName());
				}
			}
		});
	}

	private void setupCurrentPersonBinding() {
		lsvPatients.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> this.getDataModel()
						.setCurrentPerson(newSelection));
		
		this.getDataModel().getCurrentPersonProperty().addListener((obs, oldPerson, newPerson) -> {
			if (newPerson == null) {
				lsvPatients.getSelectionModel().clearSelection();
			} else {
				lsvPatients.getSelectionModel().select(newPerson);
			}
		});
	}
}
