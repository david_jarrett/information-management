package controller;

import java.io.IOException;

import application.Main;
import dal.MySqlSystemUserDAL;
import dal.SystemUserDAL;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import model.DataModel;
import model.LoginManager;
import model.SystemUser;

/**
 * Controller for the application menu bar.
 * @author David Jarrett
 * @version 09/16/2018
 *
 */
public class PrimaryViewMenuBarController extends MVPController {
	private static String PATH_TO_LOGIN_WINDOW = "../view/LoginWindow.fxml";
	private static String PATH_TO_ADMIN_PANEL = "../view/AdminPanelWindow.fxml";
	private static String PATH_TO_ADD_USER = "../view/AddUserWindow.fxml";
	
	@FXML
	private MenuBar menuBar;
	
	@FXML
	private MenuItem menuBarItem_admin;
	
	@FXML
	private MenuItem menuBarItem_addUser;
	
	/**
     * Initializes the MVPController with the main window and data model.
     * @preconditions: mainWindow != null && dataModel != null
     * @postconditions: MPVController will be initialized.
     */
	@Override
    public void initialize(Main mainApplication, DataModel dataModel) {
    	super.initialize(mainApplication, dataModel);
    	this.addAdminMenu();
	}

	private void addAdminMenu() {
		SystemUserDAL dal = new MySqlSystemUserDAL();
    	SystemUser currentUser = LoginManager.getInstance().getCurrentUser();
    	Menu menu_admin = new Menu("Admin");
    	if (dal.isAdmin(currentUser)) {
    		this.menuBar.getMenus().add(1, menu_admin);
    		this.menuBarItem_admin = new MenuItem("Open Admin Panel");
    		this.menuBarItem_addUser = new MenuItem("Add User Password");
    		
    		this.menuBar.getMenus().get(1).getItems().add(this.menuBarItem_admin);
    		this.menuBar.getMenus().get(1).getItems().add(this.menuBarItem_addUser);
    		
    		this.menuBarItem_admin.setOnAction(this.onAdminPanelClicked());
    		this.menuBarItem_addUser.setOnAction(this.onAddUserClicked());
    	}
	}
	
	private EventHandler<ActionEvent> onAdminPanelClicked() {
		return new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(getClass().getResource(PATH_TO_ADMIN_PANEL));
					Scene scene = new Scene(loader.load());
					Stage stage = new Stage();
					MVPController controller = loader.getController();
					controller.initialize(getMainApplication(), getDataModel());
					
					stage.setTitle("Admin Panel");
					stage.setScene(scene);
					stage.show();
				} catch (IOException e){
					e.printStackTrace();
				}
				
				
			}
			
		};
	}
	
	private EventHandler<ActionEvent> onAddUserClicked() {
		return new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(getClass().getResource(PATH_TO_ADD_USER));
					Scene scene = new Scene(loader.load());
					Stage stage = new Stage();
					MVPController controller = loader.getController();
					controller.initialize(getMainApplication(), getDataModel());
					
					stage.setTitle("Set Password");
					stage.setScene(scene);
					stage.show();
				} catch (IOException e){
					e.printStackTrace();
				}
				
				
			}
			
		};
	}

	@FXML
    private void onCloseClicked(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource(PATH_TO_LOGIN_WINDOW));
			Scene scene = new Scene(loader.load());
			Stage stage = new Stage();
			MVPController controller = loader.getController();
			controller.initialize(getMainApplication(), getDataModel());
			
			stage.setTitle("Login");
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.getMainApplication().getPrimaryStage().close();
    }
	
	@FXML
	private void showAbout(ActionEvent event) {
		Alert window = new Alert(AlertType.INFORMATION);
		window.setTitle("About...");
		window.setHeaderText("Healthcare System");
		window.setContentText("Information Management Project by\nRas Fincher and David Jarrett");
		window.showAndWait();
	}
}
