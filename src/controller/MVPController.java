package controller;

import java.util.Objects;

import application.Main;
import model.DataModel;

/**
 * This class acts as a superclass to all controllers used in the application. It contains some
 * useful common functionality.
 * @author David Jarrett
 * @version 09/15/2018
 */
public abstract class MVPController {

	private Main mainApplication;
	private DataModel dataModel;
	
	/**
	 * Returns a reference to the main application class.
	 * @preconditions None
	 * 
	 * @return Reference to the main application class.
	 */
	public Main getMainApplication() {
		return this.mainApplication;
	}
	
	/**
	 * Returns a reference to the application-wide data model.
	 * @preconditions None
	 * 
	 * @return A reference to the data model.
	 */
	public DataModel getDataModel() {
		return this.dataModel;
	}
	
	/**
	 * This method is called whenever a new controller is created in order to
	 * maintain references to the main application object and the data model.
	 * @preconditions (mainWindow && dataModel) != null AND the dataModel for a given controller
	 * 				  can only be initialized once.
	 * @postconditions The mainWindow and dataModel references will be stored.
	 * 
	 * @param mainWindow A reference to the main application class.
	 * @param dataModel A reference to the application-wide data model.
	 */
	public void initialize(Main mainWindow, DataModel dataModel) {
		Objects.requireNonNull(mainWindow, "mainWindow cannot be null.");
		Objects.requireNonNull(dataModel, "dataModel cannot be null.");
		this.setMainApplication(mainWindow);
		this.setDataModel(dataModel);
	}
	
	private void setMainApplication(Main mainApplication) {
		this.mainApplication = Objects.requireNonNull(mainApplication);
	}
	
	private void setDataModel(DataModel dataModel) {
		if (this.dataModel != null) {
			throw new IllegalStateException("The data model can only be initialized once.");
		}
		this.dataModel = Objects.requireNonNull(dataModel);
	}
}
