package controller;

import java.util.ArrayList;
import java.util.List;

import application.Main;
import dal.MySqlTestDAL;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.util.Callback;
import model.DataModel;
import model.Test;

/**
 * Controller for order tests view.
 * @author David Jarrett
 * @version Fall 2018
 */
public class OrderTestController extends MVPController {

    @FXML
    private ListView<Test> lstViewTests = new ListView<>();

    @FXML
    private Button btnOrderTest;
    
    private List<Test> selectedTests = new ArrayList<>(); 
    
    /**
     * Initializes the MVPController with the main window and data model.
     * @preconditions: mainWindow != null && dataModel != null
     * @postconditions: MPVController will be initialized.
     */
    @Override
    public void initialize(Main mainWindow, DataModel dataModel) {
    	super.initialize(mainWindow, dataModel);
    	MySqlTestDAL testDAL = new MySqlTestDAL();
    	var tests = testDAL.getTests();
    	this.lstViewTests.setItems(FXCollections.observableArrayList(tests));
    	
    	this.configureCellFactory();
    }

	private void configureCellFactory() {
		lstViewTests.setCellFactory(CheckBoxListCell.forListView(new Callback<Test, ObservableValue<Boolean>>() {
			@Override
            public ObservableValue<Boolean> call(Test item) {
                BooleanProperty observable = new SimpleBooleanProperty();
                observable.addListener((obs, wasSelected, isNowSelected) -> {
                	if (isNowSelected) {
                		selectedTests.add(item);
                	} else {
                		selectedTests.remove(item);
                	}
                });
                return observable ;
            }
        }));
	}
    
    @FXML
    private void onBtnOrderTestClicked(ActionEvent event) {
    	var testDAL = new MySqlTestDAL();
    	for (var test : selectedTests) {
    		testDAL.orderTestFor(this.getDataModel().getSelectedAppointmentID(), test);
    	}
    	this.btnOrderTest.getScene().getWindow().hide();
    }

}
