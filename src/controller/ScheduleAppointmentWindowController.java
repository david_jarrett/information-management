package controller;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import application.Main;
import dal.DoctorDAL;
import dal.MySqlAppointmentDAL;
import dal.MySqlDoctorDAL;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.DataModel;
import model.LoginManager;
import model.PatientLite;
import model.SystemUser;

/**
 * Controller for scheduling appointments
 * 
 * @author Ras Fincher & David Jarrett
 *
 */
public class ScheduleAppointmentWindowController extends MVPController {
	private static final int DUPLICATE_ENTRY_ERROR_CODE = 1062;
    
	private final List<String> times = new ArrayList<>(Arrays.asList(
    		"9:00", "10:00", "11:00", "12:00", "1:00", "2:00", "3:00", "4:00"));
    private BooleanBinding canSubmit;
	
	@FXML
    private Label patientNameLabel;
    @FXML
    private Label doctorLabel;
    @FXML
    private DatePicker apptDatePicker;
    @FXML
    private Label dateLabel;
    @FXML
    private Label timeLabel;
    @FXML
    private Label reasonLabel;
    @FXML
    private ComboBox<String> doctorComboBox;
    @FXML
    private ComboBox<String> timeComboBox;
    @FXML
    private TextArea reasonTextArea;
    @FXML
    private TextField patientNameTextField;
    @FXML
    private Button cancelButton;
    @FXML
    private Button submitButton;

    /**
     * Initializes the MVPController with the main window and data model.
     * @preconditions: mainWindow != null && dataModel != null
     * @postconditions: MPVController will be initialized.
     */
    @Override
    public void initialize(Main mainWindow, DataModel dataModel) {
    	super.initialize(mainWindow, dataModel);
    	PatientLite patientLite = dataModel.getCurrentPerson();
    	patientNameTextField.setText(patientLite.getFullName());
    	doctorComboBox.setItems(FXCollections.observableArrayList(this.getDoctors().keySet()));
    	timeComboBox.setItems(FXCollections.observableArrayList(times));
    	this.configureDatePicker();
    	this.setupValidationBinding();
    	this.doctorComboBox.requestFocus();
    }
    
    private Map<String, SystemUser> getDoctors() {
    	DoctorDAL dal = new MySqlDoctorDAL();
    	return dal.getDoctors();
    }
    
    
    @FXML
    private void onSubmitClicked(ActionEvent event) throws ParseException {
    	int patientID = this.getDataModel().getCurrentPerson().getPatientID();
    	int nurseID = Integer.parseInt(LoginManager.getInstance().getCurrentUser().getUserID());
    	int doctorID = Integer.parseInt(this.getDataModel().getDoctorMap().get(doctorComboBox.getValue()).getUserID());
    	Timestamp sqlDate = formatTime();
    	String reason = reasonTextArea.getText();
    	
    	try {
    		new MySqlAppointmentDAL().addAppointment(patientID, nurseID, doctorID, sqlDate, reason);
    		Stage stage = (Stage) submitButton.getScene().getWindow();
        	stage.close();
    	} catch (SQLException e){
    		if (e.getErrorCode() == DUPLICATE_ENTRY_ERROR_CODE) {
				handleDuplicateEntryError();
			} else {
				e.printStackTrace();
			}
    	}
    }
    
    @FXML
    private void onCancelClicked(ActionEvent event) {
    	Stage stage = (Stage) this.cancelButton.getScene().getWindow();
    	stage.close();
    }

	private Timestamp formatTime() throws ParseException {
		int hour = Integer.parseInt(timeComboBox.getValue().split(":")[0]);
		int minute = Integer.parseInt(timeComboBox.getValue().split(":")[1]);
    	LocalDateTime d = apptDatePicker.getValue().atTime(hour, minute);
    	Long date = d.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    	Timestamp sqlDate = new Timestamp(date);
		return sqlDate;
	}
    
    private void setupValidationBinding() {
    	this.canSubmit = new BooleanBinding() {
    		{
    			super.bind(patientNameTextField.textProperty(), doctorComboBox.valueProperty(),
    					apptDatePicker.valueProperty(), timeComboBox.valueProperty(), 
    					reasonTextArea.textProperty());
    		}
    		
			@Override
			protected boolean computeValue() {
				return patientNameTextField.getText().isEmpty()
						|| doctorComboBox.getValue() == null
						|| apptDatePicker.getValue() == null
						|| timeComboBox.getValue() == null
						|| reasonTextArea.getText().isEmpty();
			}
    		
    	};
    	this.submitButton.disableProperty().bind(this.canSubmit);
    }
    
    private void handleDuplicateEntryError() {
    	Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Duplicate Entry Error");
		alert.setHeaderText("Appointment Time Not Available");
		alert.setContentText("An appointment already exists on that date and time."
				+ " Please try another.");
		alert.showAndWait();
		this.apptDatePicker.requestFocus();
	}
    
    private void configureDatePicker() {
		this.apptDatePicker.setConverter(new StringConverter<LocalDate>() {
			String pattern = "MM-dd-yyyy";
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
			
			{
				apptDatePicker.setPromptText(pattern.toLowerCase());
			}
			
			@Override
			public LocalDate fromString(String string) {
				if (string != null && !string.isEmpty()) {
					return LocalDate.parse(string, dateFormatter);
				} else {
		             return null;
		         }
			}

			@Override
			public String toString(LocalDate date) {
				if (date != null) {
		             return dateFormatter.format(date);
		         } else {
		             return "";
		         }
			}
			
		});
		
		this.apptDatePicker.setDayCellFactory(picker -> new DateCell() {
	        public void updateItem(LocalDate date, boolean empty) {
	            super.updateItem(date, empty);
	            LocalDate today = LocalDate.now();

	            setDisable(empty || date.compareTo(today) < 0 || 
	            		date.getDayOfWeek() == DayOfWeek.SATURDAY ||
	            		date.getDayOfWeek() == DayOfWeek.SUNDAY);
	        }
	    });
		this.addListenerToDatePicker();
	}

	private void addListenerToDatePicker() {
		this.apptDatePicker.focusedProperty().addListener((ov, oldValue, newValue) -> {
			if (!newValue) {
				try {
					this.apptDatePicker.setValue(apptDatePicker.getConverter().fromString(apptDatePicker.getEditor().getText()));
				} catch (DateTimeParseException e) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Input Error");
					alert.setHeaderText("Invalid Date Entered");
					alert.setContentText(apptDatePicker.getEditor().getText() + " is not a valid date. "
							+ "Please enter a date in the formate MM-DD-YYYY");
					alert.showAndWait();
					this.apptDatePicker.requestFocus();
				}
				
			}
		});
	}
}
