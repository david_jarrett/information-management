package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import application.Main;
import dal.AppointmentDAL;
import dal.MySqlAppointmentDAL;
import dal.MySqlPatientDAL;
import dal.MySqlVisitDAL;
import dal.MySqlVisitDAL.NoVisitException;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import model.BPReading;
import model.DataModel;
import model.LoginManager;
import model.PatientLite;
import model.SystemUser;
import model.Visit;
import util.ValidationTools;

/**
 * Controller for creating a visit and entering routine checks
 * 
 * @author Ras Fincher & David Jarrett
 *
 */
public class RoutineChecksWindowController extends MVPController {
	private static final int DUPLICATE_ENTRY_ERROR_CODE = 1062;
	private static final String ORDER_TEST_WINDOW_PATH = "../view/OrderTestsWindow.fxml";
	private static final String VIEW_TEST_WINDOW_PATH = "../view/ViewTestsWindow.fxml";

	private BooleanBinding canSubmit;
	private MySqlVisitDAL visitDAL;
	private MySqlPatientDAL patientDAL;
	private MySqlAppointmentDAL apptDAL;
	private SystemUser currentUser;
	private PatientLite currentPatient;
	private Visit currentVisit;
	private boolean closeWindow = true;
	
	@FXML
	private Label lbl_name;
	@FXML
	private Label lbl_appt;
	@FXML
	private Label lbl_sys;
	@FXML
	private Label lbl_dia;
	@FXML
	private Label lbl_pulse;
	@FXML
	private Label lbl_temp;
	@FXML
	private Label lbl_weight;
	@FXML
	private Label lbl_finalDiag;
	@FXML
	private Label lbl_initDiag;
	@FXML
	private TextField txtField_name;
	@FXML
	private TextField txtField_doctor;
	@FXML
	private TextField txtField_diastolic;
	@FXML
	private TextField txtField_systolic;
	@FXML
	private TextField txtField_pulse;
	@FXML
	private TextField txtField_temp;
	@FXML
	private TextField txtField_weight;
	@FXML
	private Button btn_cancel;
	@FXML
	private Button btn_submit;
	@FXML
	private Button btnOrderTests;
	@FXML
	private Button btnViewTests;
	@FXML
	private TextArea txtArea_initDiag;
	@FXML
	private TextArea txtArea_finalDiag;
	@FXML
	private ComboBox<String> comboBox_appt;

	/**
     * Initializes the MVPController with the main window and data model.
     * @preconditions: mainWindow != null && dataModel != null
     * @postconditions: MPVController will be initialized.
     */
	@Override
	public void initialize(Main mainWindow, DataModel dataModel) {
		super.initialize(mainWindow, dataModel);
		this.visitDAL = new MySqlVisitDAL();
		this.patientDAL = new MySqlPatientDAL();
		this.apptDAL = new MySqlAppointmentDAL();
		this.currentUser = LoginManager.getInstance().getCurrentUser();
		this.currentPatient = dataModel.getCurrentPerson();
		this.txtField_name.setText(this.currentPatient.getFullName());
		this.setupBindings();
		this.populateApptComboBox(this.currentPatient.getPatientID());
		this.addListenerToApptComboBox();
		this.comboBox_appt.requestFocus();

		this.btnOrderTests.disableProperty().bind(this.canSubmit);
		this.btnViewTests.disableProperty().bind(this.canSubmit);
	}

	@FXML
	void btnOrderTestsClicked(ActionEvent event) throws SQLException, NoVisitException {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource(ORDER_TEST_WINDOW_PATH));
			Scene scene = new Scene(loader.load());
			Stage stage = new Stage();
			MVPController controller = loader.getController();
			this.getDataModel().setSelectedAppointmentID(currentVisit.getAppointmentID());
			controller.initialize(getMainApplication(), getDataModel());

			stage.setTitle("Order Test");
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			this.closeWindow = false;
			this.onSubmitClicked(event);
			this.fetchExistingVisitInfo();
			this.currentPatient = this.getDataModel().getCurrentPerson();
			this.btnOrderTestsClicked(event);
			this.closeWindow = true;
		}
	}

	@FXML
	void btnTestResultsClicked(ActionEvent event) throws IOException {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource(VIEW_TEST_WINDOW_PATH));
			Scene scene = new Scene(loader.load());
			Stage stage = new Stage();
			ViewTestsController controller = loader.getController();
			this.getDataModel().setSelectedAppointmentID(currentVisit.getAppointmentID());
			controller.initialize(getMainApplication(), getDataModel());
			if (this.btn_submit.isDisabled()) {
				controller.lockWindow();
			}

			stage.setTitle("View Tests");
			stage.setScene(scene);
			stage.show();
		} catch (NullPointerException e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("No Tests");
			alert.setContentText("No tests have been ordered for this patient yet.");
			alert.showAndWait();
		}
	}

	@FXML
	void onSubmitClicked(ActionEvent event) throws SQLException {
		int patientID = this.currentPatient.getPatientID();
		int nurseID = Integer.parseInt(this.currentUser.getUserID());
		Map<String, Timestamp> appts = this.patientDAL.getPatientAppointments(patientID);
		Timestamp date = appts.get(this.comboBox_appt.getValue());
		int apptID = this.apptDAL.getAppointmentID(patientID, date);
		int sysBP = Integer.parseInt(this.txtField_systolic.getText());
		int diaBP = Integer.parseInt(this.txtField_diastolic.getText());
		double temp = Double.parseDouble(this.txtField_temp.getText());
		int pulse = Integer.parseInt(this.txtField_pulse.getText());
		double weight = Double.parseDouble(this.txtField_weight.getText());
		String initialDiagnosis = this.txtArea_initDiag.getText();
		String finalDiagnosis = this.txtArea_finalDiag.getText();

		if (!finalDiagnosis.isEmpty() && !this.displayCloseWarning()) {
			return;
		}

		try {
			this.visitDAL.createVisit(apptID, patientID, nurseID, sysBP, diaBP, temp, pulse, weight, initialDiagnosis,
					finalDiagnosis);
		} catch (SQLException e) {
			if (e.getErrorCode() == DUPLICATE_ENTRY_ERROR_CODE) {
				this.doVisitUpdate();
			} else {
				e.printStackTrace();
			}
		}

		if (closeWindow) {
			this.closeWindow(btn_submit);
		}
	}

	private boolean displayCloseWarning() {
		ButtonType yes = new ButtonType("Yes");
		ButtonType no = new ButtonType("No");
		String message = "You entered a final diagnosis. This will close the current visit, and any further edits will be impossible. Are you sure?";
		Alert alert = new Alert(AlertType.WARNING, message, yes, no);
		alert.setTitle("Closing Visit");

		boolean[] result = { true };

		alert.showAndWait().ifPresent(response -> {
			if (response == no) {
				result[0] = false;
			}
		});

		return result[0];
	}

	private void doVisitUpdate() {
		Visit visit = this.makeVisit();
		this.visitDAL.updateVisit(visit);
	}

	private Visit makeVisit() {
		int aptID = this.getAppointmentID();
		int patientID = this.currentVisit.getPatientID();
		int nurseID = this.currentVisit.getNurseID();
		int systolic = Integer.parseInt(this.txtField_systolic.getText());
		int diastolic = Integer.parseInt(this.txtField_diastolic.getText());
		double temp = Double.parseDouble(this.txtField_temp.getText());
		int pulse = Integer.parseInt(this.txtField_pulse.getText());
		double weight = Double.parseDouble(this.txtField_weight.getText());
		String initDiag = this.txtArea_initDiag.getText();
		String finalDiag = this.txtArea_finalDiag.getText();

		return new Visit(aptID, patientID, nurseID, systolic, diastolic, temp, pulse, weight, initDiag, finalDiag);
	}

	@FXML
	private void onCancelClicked(ActionEvent event) {
		this.closeWindow(btn_cancel);
	}

	private void populateApptComboBox(int patientID) {
		Map<String, Timestamp> m = this.patientDAL.getPatientAppointments(patientID);
		var obsvList = FXCollections.observableArrayList(m.keySet());
		obsvList.sort(null);
		this.comboBox_appt.setItems(obsvList);
	}

	private void addListenerToApptComboBox() {
		this.comboBox_appt.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
			int apptID = this.getAppointmentID();
			AppointmentDAL dal = new MySqlAppointmentDAL();
			String doctor = "";
			try {
				doctor = dal.getDoctorName(apptID);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			this.txtField_doctor.setText(doctor);

		});
	}

	@FXML
	private void updateVisitData(ActionEvent event) {
		try {
			fetchExistingVisitInfo();
			doBloodPressureCheck();
		} catch (NoVisitException e) {
			this.clearAllTextFields();
		}
	}

	private void fetchExistingVisitInfo() throws NoVisitException {
		int aptID = this.getAppointmentID();
		Visit visit = this.visitDAL.getVisit(aptID);
		this.currentVisit = visit;

		this.txtField_systolic.setText(String.valueOf(visit.getSystolic()));
		this.txtField_diastolic.setText(String.valueOf(visit.getDiastolic()));
		this.txtField_pulse.setText(String.valueOf(visit.getPulse()));
		this.txtField_temp.setText(String.valueOf(visit.getTemp()));
		this.txtField_weight.setText(String.valueOf(visit.getWeight()));
		this.txtArea_initDiag.setText(visit.getInitialDiagnosis());
		this.txtArea_finalDiag.setText(visit.getFinalDiagnosis());

		if (!this.txtArea_finalDiag.getText().isEmpty()) {
			this.disableAllFields();
		}
	}

	private void doBloodPressureCheck() {
		int high = 0;
		int elevated = 0;
		List<BPReading> readings = this.patientDAL.getRecentBPHistory(this.currentPatient.getPatientID());
		if (readings.size() < 3) {
			return;
		}

		for (var reading : readings) {
			switch (reading.getStatus()) {
			case "High":
				high++;
				break;
			case "Elevated":
				elevated++;
				break;
			}
		}

		if (high >= 3) {
			showBPWarning("High", readings);
		} else if (elevated >= 3) {
			showBPWarning("Elevated", readings);
		} else if (high + elevated >= 3) {
			showBPWarning("Elevated or High", readings);
		}
	}

	private void showBPWarning(String message, List<BPReading> readings) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Blood Pressure Warning");

		StringBuilder output = new StringBuilder();
		output.append("Patient may have ").append(message).append(" blood pressure.").append(System.lineSeparator());
		output.append("Last three readings:").append(System.lineSeparator());
		for (var reading : readings) {
			output.append(reading.getSystolic() + "/" + reading.getDiastolic()).append(System.lineSeparator());
		}

		alert.setContentText(output.toString());
		alert.showAndWait();
	}

	private void disableAllFields() {
		this.txtField_systolic.setDisable(true);
		this.txtField_diastolic.setDisable(true);
		this.txtField_pulse.setDisable(true);
		this.txtField_temp.setDisable(true);
		this.txtField_weight.setDisable(true);
		this.txtArea_initDiag.setDisable(true);
		this.txtArea_finalDiag.setDisable(true);

		this.btn_submit.disableProperty().unbind();
		this.btn_submit.setDisable(true);
		this.btnOrderTests.disableProperty().unbind();
		this.btnOrderTests.setDisable(true);
		this.btn_cancel.setText("Close");
	}

	private void clearAllTextFields() {
		this.txtField_systolic.setText("");
		this.txtField_diastolic.setText("");
		this.txtField_pulse.setText("");
		this.txtField_temp.setText("");
		this.txtField_weight.setText("");
		this.txtArea_initDiag.setText("");
		this.txtArea_finalDiag.setText("");
	}

	private int getAppointmentID() {
		String appointment = this.comboBox_appt.getSelectionModel().getSelectedItem();
		int leftBracket = appointment.indexOf('<');
		int rightBracket = appointment.indexOf('>');
		String aptID = appointment.substring(leftBracket + 1, rightBracket);
		return Integer.parseInt(aptID);
	}

	private void setupBindings() {
		this.canSubmit = new BooleanBinding() {
			{
				super.bind(txtField_diastolic.textProperty(), txtField_systolic.textProperty(),
						txtField_pulse.textProperty(), txtField_temp.textProperty(), txtField_weight.textProperty(),
						txtArea_initDiag.textProperty(), comboBox_appt.valueProperty());
			}

			@Override
			protected boolean computeValue() {
				updateApptFieldColor();
				updateSysBPFieldColor();
				updateDiaBPFieldColor();
				updatePulseFieldColor();
				updateTempFieldColor();
				updateWeightFieldColor();
				updateInitDiagFieldColor();
				updateFinalDiagFieldColor();
				return txtField_diastolic.getText().isEmpty() || txtField_systolic.getText().isEmpty()
						|| txtField_pulse.getText().isEmpty() || txtField_temp.getText().isEmpty()
						|| txtField_weight.getText().isEmpty() || txtArea_initDiag.getText().isEmpty()
						|| comboBox_appt.getValue() == null
						|| !ValidationTools.validateSysBP(txtField_systolic.getText())
						|| !ValidationTools.validateDiaBP(txtField_diastolic.getText())
						|| !ValidationTools.validatePulse(txtField_pulse.getText())
						|| !ValidationTools.validateTemp(txtField_temp.getText())
						|| !ValidationTools.validateWeight(txtField_weight.getText());
			}

		};
		this.btn_submit.disableProperty().bind(this.canSubmit);
	}

	private void updateApptFieldColor() {
		if (!(this.comboBox_appt.getValue() == null)) {
			this.lbl_appt.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.lbl_appt.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateSysBPFieldColor() {
		if (ValidationTools.validateSysBP(this.txtField_systolic.getText())) {
			this.lbl_sys.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.lbl_sys.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateDiaBPFieldColor() {
		if (ValidationTools.validateDiaBP(this.txtField_diastolic.getText())) {
			this.lbl_dia.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.lbl_dia.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updatePulseFieldColor() {
		if (ValidationTools.validatePulse(this.txtField_pulse.getText())) {
			this.lbl_pulse.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.lbl_pulse.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateTempFieldColor() {
		if (ValidationTools.validateTemp(this.txtField_temp.getText())) {
			this.lbl_temp.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.lbl_temp.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateWeightFieldColor() {
		if (ValidationTools.validateWeight(this.txtField_weight.getText())) {
			this.lbl_weight.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.lbl_weight.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateInitDiagFieldColor() {
		if (this.txtArea_initDiag.getText().length() > 1000 || (!(this.txtArea_initDiag.getText().isEmpty()))) {
			this.lbl_initDiag.textFillProperty().set(Paint.valueOf("GREEN"));
		} else {
			this.lbl_initDiag.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void updateFinalDiagFieldColor() {
		if (this.txtArea_finalDiag.getText().length() < 1000) {
			this.lbl_finalDiag.textFillProperty().set(Paint.valueOf("BLACK"));
		} else {
			this.lbl_finalDiag.textFillProperty().set(Paint.valueOf("RED"));
		}
	}

	private void closeWindow(Button button) {
		Stage stage = (Stage) button.getScene().getWindow();
		stage.close();
	}
}
