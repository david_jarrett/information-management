package controller;

import java.io.IOException;

import application.Main;
import dal.MySqlPatientDAL;
import dal.MySqlSystemUserDAL;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.DataModel;
import model.LoginManager;
import model.SystemUser;

public class PrimaryViewInformationBarController extends MVPController {

	private static String PATH_TO_PATIENT_MAINTENANCE_WINDOW = "../view/PatientMaintenanceWindow.fxml";
	private static String PATH_TO_SCHEDULE_APPOINTMENT_WINDOW = "../view/ScheduleAppointmentWindow.fxml";
	private static String PATH_TO_ROUTINE_CHECKS_WINDOW = "../view/RoutineChecksWindow.fxml";
	private static String PATH_TO_SEARCH_BOX = "../view/SearchWindow.fxml";
	
	@FXML
	private Button btnAddPatient;
	
	@FXML
	private Button btnEditPatient;
	
	@FXML
	private Button btnSearch;
	
	@FXML
	private Button btnScheduleAppt;
	
	@FXML
	private Button btnRoutineCheck;
	
	@FXML
    private Text lblLoggedIn;
	
	private BooleanBinding canEdit;
	
	private BooleanBinding canAddAppointment;
	
	private BooleanBinding canAddRoutineCheck;
	
	/**
	 * Initializes the controller for the information bar. Currently, this bar displays the name and
	 * ID number of the currently logged in user.
	 * @preconditions (mainWindow && dataModel) != null
	 * @postconditions 	The controller will be initialized with the main window reference and data model. Also, the
	 * 					currently logged in user's information string will be bound to the information bar.
	 */
	@Override
	public void initialize(Main mainWindow, DataModel dataModel) {
		super.initialize(mainWindow, dataModel);
		this.lblLoggedIn.textProperty().bind(LoginManager.getInstance().getCurrentUser().getInformationStringProperty());
		this.setUpSelectedPersonBindings(this.canEdit, this.btnEditPatient);
		this.setUpSelectedPersonBindings(this.canAddAppointment, this.btnScheduleAppt);
		this.setUpSelectedPersonBindings(this.canAddRoutineCheck, this.btnRoutineCheck);
		
	}
		
	@FXML
    void addPatient(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource(PATH_TO_PATIENT_MAINTENANCE_WINDOW));
			Scene scene = new Scene(loader.load());
			Stage stage = new Stage();
			MVPController controller = loader.getController();
			controller.initialize(getMainApplication(), getDataModel());
			
			stage.setTitle("Add New Patient");
			stage.setScene(scene);
			stage.show();
		} catch (IOException e){
			e.printStackTrace();
		}
    }
	
	@FXML
    void editPatient(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource(PATH_TO_PATIENT_MAINTENANCE_WINDOW));
			Scene scene = new Scene(loader.load());
			Stage stage = new Stage();
			MVPController controller = loader.getController();
			controller.initialize(getMainApplication(), getDataModel());
			((PatientMaintenanceWindowController) controller).editPatient(true);
			
			stage.setTitle("Edit Patient");
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	@FXML
	void openSearchWindow(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource(PATH_TO_SEARCH_BOX));
			Scene scene = new Scene(loader.load());
			Stage stage = new Stage();
			MVPController controller = loader.getController();
			controller.initialize(getMainApplication(), getDataModel());
						
			stage.setTitle("Search");
			stage.setScene(scene);
			stage.show();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	@FXML
	void scheduleAppointment(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource(PATH_TO_SCHEDULE_APPOINTMENT_WINDOW));
			Scene scene = new Scene(loader.load());
			Stage stage = new Stage();
			MVPController controller = loader.getController();
			controller.initialize(getMainApplication(), getDataModel());
			
			stage.setTitle("Schedule Appointment");
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	void openRoutineCheck(ActionEvent event) {
		if (!this.patientHasAppointment()) {
			Alert noApptAlert = new Alert(AlertType.ERROR, "Current patient has no appointments."
					+ " Please create an appointment.");
			noApptAlert.showAndWait();
		} else {
			try {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource(PATH_TO_ROUTINE_CHECKS_WINDOW));
				Scene scene = new Scene(loader.load());
				Stage stage = new Stage();
				MVPController controller = loader.getController();
				controller.initialize(getMainApplication(), getDataModel());
				
				stage.setTitle("Routine Check");
				stage.setScene(scene);
				stage.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void setUpSelectedPersonBindings(BooleanBinding boolBind, Button button) {
		SystemUser current = LoginManager.getInstance().getCurrentUser();
		MySqlSystemUserDAL dal = new MySqlSystemUserDAL();
		
		if (dal.isNurse(current)) {
			boolBind = new BooleanBinding() {
				{
					super.bind(DataModel.getInstance().getCurrentPersonProperty());
				}
				
				@Override
				protected boolean computeValue() {
					button.disabledProperty();
					
					return DataModel.getInstance().getCurrentPerson() == null;
				}
			};
			button.disableProperty().bind(boolBind);
		} else {
			this.disableButtons();
		}
	}
	
	private void disableButtons() {
		this.btnAddPatient.setDisable(true);
		this.btnEditPatient.setDisable(true);
		this.btnRoutineCheck.setDisable(true);
		this.btnScheduleAppt.setDisable(true);
	}
	
	private boolean patientHasAppointment() {
		return new MySqlPatientDAL().getPatientAppointments(
				DataModel.getInstance().getCurrentPerson().getPatientID()).size() > 0;
	}
}
