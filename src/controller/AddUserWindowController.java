package controller;

import java.sql.SQLException;

import dal.LoginDAL;
import dal.MySqlLoginDAL;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * Controller for the add user window.
 * @author David Jarrett & Ras Fincher
 * @date Fall 2018
 */
public class AddUserWindowController extends MVPController {
	
	@FXML
    private TextField txtField_username;

    @FXML
    private PasswordField pwdField_password;

    @FXML
    private Button btn_submit;
    
    @FXML
    void submitClicked(ActionEvent event) {
    	LoginDAL dal = new MySqlLoginDAL();
    	String username = this.txtField_username.getText();
    	String password = this.pwdField_password.getText();
    	
    	try {
			dal.setPassword(username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	this.btn_submit.getScene().getWindow().hide();
    }
}
