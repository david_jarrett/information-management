package util;

/**
 * Utility class used to validate certain user information.
 * @author Ras Fincher
 * @version 09/18/2018
 */
public class ValidationTools {
	
	public static final String TEN_DIGIT_PHONE_NO_CHARACTERS = "\\d{10}";
	public static final String TEN_DIGIT_PHONE_WITH_SEPARATORS = "\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}";
	public static final String TEN_DIGIT_PHONE_WITH_PARENS_AND_SEPARATORS = "\\(\\d{3}\\)[\\s]?\\d{3}-\\d{4}";
	
	public static final String FIVE_DIGIT_ZIP = "\\d{5}";
	public static final String NINE_DIGIT_ZIP = "\\d{5}-\\d{4}";
	
	public static final String SSN_NO_DASHES = "\\d{9}";
	public static final String SSN_WITH_DASHES = "\\d{3}-\\d{2}-\\d{4}";
	
	public static final String VALID_VITALS = "\\d{1,3}";
	public static final String VALID_TEMP = "\\d{1,3}\\.\\d{1,2}";
	public static final String VALID_WEIGHT = "\\d{1,4}\\.\\d{1,3}";
	
	
	
	/**
	 * Returns true if passed a valid phone number, false otherwise.
	 * @param phoneNumber The phone number.
	 * @return true if valid phone number, false otherwise.
	 */
	public static boolean validatePhoneNumber(String phoneNumber) {
		return phoneNumber.matches(ValidationTools.TEN_DIGIT_PHONE_NO_CHARACTERS) ||
				phoneNumber.matches(ValidationTools.TEN_DIGIT_PHONE_WITH_SEPARATORS) ||
				phoneNumber.matches(ValidationTools.TEN_DIGIT_PHONE_WITH_PARENS_AND_SEPARATORS);
	}
	
	/**
	 * Returns true if passed a valid zip code, false otherwise.
	 * @param zipCode The zip code.
	 * @return true if passed a valid zip code, false otherwise.
	 */
	public static boolean validateZipCode(String zipCode) {
		return zipCode.matches(ValidationTools.FIVE_DIGIT_ZIP) ||
				zipCode.matches(ValidationTools.NINE_DIGIT_ZIP);
	}
	
	/**
	 * Returns true if passed a valid SSN, false otherwise.
	 * @param zipCode The SSN.
	 * @return true if passed a valid SSN, false otherwise.
	 */
	public static boolean validateSSN(String ssn) {
		return ssn.matches(ValidationTools.SSN_NO_DASHES) ||
				ssn.matches(ValidationTools.SSN_WITH_DASHES);
	}
	
	/**
	 * Returns true if passed a valid blood pressure number,
	 * false otherwise.
	 * @param sysBP the systolic blood pressure number
	 * @return true if passed a valid blood pressure number,
	 * false otherwise.
	 */
	public static boolean validateSysBP(String sysBP) {
		return sysBP.matches(VALID_VITALS);
	}
	
	/**
	 * Returns true if passed a valid blood pressure number,
	 * false otherwise.
	 * @param sysBP the dyastolic blood pressure number
	 * @return true if passed a valid blood pressure number,
	 * false otherwise.
	 */
	public static boolean validateDiaBP(String diaBP) {
		return diaBP.matches(VALID_VITALS);
	}
	
	/**
	 * Returns true if passed a valid temperature with two decimal 
	 * places, false otherwise.
	 * @param sysBP the temperature
	 * @return true if passed a valid temperature with two decimal 
	 * places, false otherwise.
	 */
	public static boolean validateTemp(String temp) {
		return temp.matches(VALID_TEMP);
	}
	
	/**
	 * Returns true if passed a valid pulse number
	 * @param pulse the pulse in bpm
	 * @return true if passed a valid pulse number
	 */
	public static boolean validatePulse(String pulse) {
		return pulse.matches(VALID_VITALS);
	}
	
	/**
	 * Returns true if passed a valid weight with up to 
	 * three decimal places, false otherwise.
	 * @param weight the patient's weight in lbs
	 * @return true if passed a valid weight with up to 
	 * three decimal places, false otherwise.
	 */
	public static boolean validateWeight(String weight) {
		return weight.matches(VALID_WEIGHT);
	}
}
