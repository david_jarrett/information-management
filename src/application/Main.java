package application;

import controller.MVPController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.DataModel;

/**
 * This class represents the primary application.
 * @author David Jarrett
 * @version 09/15/2018
 */
public class Main extends Application {
	
	private Stage theStage;
	private DataModel dataModel;
	
	/**
	 * JavaFX application entry-point. Displays the initial login screen.
	 * @preconditions None
	 * @postconditions None
	 * 
	 * @param primaryStage The stage used throughout the application to display the UI.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			this.theStage = primaryStage;
			this.dataModel = DataModel.getInstance();
			
			BorderPane root = new BorderPane();
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/LoginWindow.fxml"));
			root.setCenter(loader.load());
			MVPController controller = loader.getController();
			controller.initialize(this, this.dataModel);
												
			Scene scene = new Scene(root,350,200);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Healthcare System Login");
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the application's primary stage.
	 * @preconditions None
	 * @return The primary stage.
	 */
	public Stage getPrimaryStage() {
		return this.theStage;
	}
	
	/**
	 * Entry-point for the JVM.
	 * @preconditions None
	 * @postconditions None
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
