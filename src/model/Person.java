package model;

import java.time.LocalDate;
import java.util.Objects;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This abstract class represents generic information that can apply to both client's and employees.
 * @author David Jarrett
 * @version 09/15/2018
 */
public abstract class Person {
	
	private StringProperty firstName = new SimpleStringProperty(this, "firstName", "");
	private StringProperty lastName = new SimpleStringProperty(this, "firstName", "");;
	private ObjectProperty<LocalDate> dateOfBirth = new SimpleObjectProperty<>(this, "dateOfBirth", null);
	private ObjectProperty<Address> address = new SimpleObjectProperty<>(this, "address", null);
	private StringProperty phone = new SimpleStringProperty(this, "phone", "");
	private StringProperty gender = new SimpleStringProperty(this, "gender", "");
	private StringProperty ssn = new SimpleStringProperty(this, "ssn", "");
		
	/**
	 * Initializes the object.
	 * @preconditions (firstName, lastName, dob, address, phone) != null
	 * @postconditions Object is initialized.
	 * 
	 * @param firstName User's first name
	 * @param lastName User's last name
	 * @param dob User's date of birth
	 * @param address User's address
	 * @param phone User's phone number
	 */
	public Person(String firstName, String lastName, LocalDate dob, Address address, String phone, String gender, String ssn) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setDateOfBirth(dob);
		this.setAddress(address);
		this.setPhone(phone);
		this.setGender(gender);
		this.setSSN(ssn);
	}
	
	/**
	 * Returns the first name.
	 * @preconditions None
	 * 
	 * @return The first name.
	 */
	public final String getFirstName() {
		return this.firstName.get();
	}
	
	/**
	 * Sets the first name.
	 * @preconditions firstName != null
	 * @postconditions this.firstName == firstName
	 * 
	 * @param firstName The new first name.
	 */
	public final void setFirstName(String firstName) {
		Objects.requireNonNull(firstName, "firstName cannot be null.");
		this.firstName.set(firstName);
	}
	
	/**
	 * Return the property containing the first name.
	 * @preconditions None
	 * 
	 * @return The firstName property.
	 */
	public StringProperty getFirstNameProperty() {
		return this.firstName;
	}
	
	/**
	 * Return the last name.
	 * @preconditions None
	 * 
	 * @return The last name.
	 */
	public final String getLastName() {
		return this.lastName.get();
	}
	
	/**
	 * Sets the last name.
	 * @preconditions lastName != null
	 * 
	 * @param lastName The new last name.
	 */
	public final void setLastName(String lastName) {
		Objects.requireNonNull(lastName, "lastName cannot be null.");
		this.lastName.set(lastName);
	}
	
	/**
	 * Returns the property containing the last name.
	 * @preconditions None
	 * 
	 * @return The lastName property.
	 */
	public StringProperty getLastNameProperty() {
		return this.lastName;
	}
	
	/**
	 * Return the date of birth.
	 * @preconditions None
	 * 
	 * @return The date of birth.
	 */
	public final LocalDate getDateOfBirth() {
		return this.dateOfBirth.get();
	}
	
	/**
	 * Sets the date of birth.
	 * @preconditions dob != null
	 * 
	 * @param dob The date of birth.
	 */
	public final void setDateOfBirth(LocalDate dob) {
		this.dateOfBirth.set(Objects.requireNonNull(dob, "dob cannot be null"));
	}
	
	/**
	 * Return the property that contains the date of birth.
	 * @preconditions None
	 * 
	 * @return The dateOfBirth property.
	 */
	public ObjectProperty<LocalDate> getDateOfBirthProperty() {
		return this.dateOfBirth;
	}
	
	/**
	 * Return the address.
	 * @preconditions None
	 * 
	 * @return The address.
	 */
	public final Address getAddress() {
		return this.address.get();
	}
	
	/**
	 * Sets the address.
	 * @preconditions address != null
	 * 
	 * @param address The address
	 */
	public final void setAddress(Address address) {
		this.address.set(Objects.requireNonNull(address, "address cannot be null"));
	}
	
	/**
	 * Returns the property that contains the address.
	 * @preconditions None
	 * 
	 * @return The address property.
	 */
	public ObjectProperty<Address> getAddressProperty() {
		return this.address;
	}
	
	/**
	 * Returns the phone number.
	 * @preconditions None
	 * 
	 * @return The phone number.
	 */
	public final String getPhone() {
		return this.phone.get();
	}
	
	/**
	 * Sets the phone number.
	 * @preconditions phoneNumber != null
	 * 
	 * @param phone The phone number.
	 */
	public final void setPhone(String phone) {
		this.phone.set(Objects.requireNonNull(phone, "phone cannot be null"));
	}
	
	/**
	 * Return the property that contains the phone number.
	 * @preconditions None
	 * 
	 * @return The phone property.
	 */
	public StringProperty getPhoneProperty() {
		return this.phone;
	}
	
	/**
	 * Returns the gender.
	 * @preconditions None
	 * 
	 * @return The gender of the person.
	 */
	public String getGender() {
		return this.gender.get();
	}
	
	/**
	 * Sets the gender.
	 * @preconditions gender != null
	 * @postconditions this.gender == gender
	 * 
	 * @param gender The person's gender
	 */
	public void setGender(String gender) {
		this.gender.set(Objects.requireNonNull(gender, "gender cannot be null"));
	}
	
	/**
	 * Gets the gender property.
	 * @preconditions None
	 *
	 * @return The gender property
	 */
	public StringProperty getGenderProperty() {
		return this.gender;
	}
	
	/**
	 * Gets the person's SSN.
	 * @preconditions None
	 * 
	 * @return The person's SSN.
	 */
	public String getSSN() {
		return this.ssn.get();
	}
	
	/**
	 * Set the SSN
	 * @preconditions ssn != null
	 * 
	 * @param ssn The person's SSN.
	 */
	public void setSSN(String ssn) {
		this.ssn.set(Objects.requireNonNull(ssn, "ssn cannot be null"));
	}
	
	/**
	 * Returns the SSN property.
	 * @preconditions None
	 * 
	 * @return The SSN property.
	 */
	public StringProperty getSSNProperty() {
		return this.ssn;
	}

}
