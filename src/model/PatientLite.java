package model;

/**
 * Describes a lite version of a Patient to be used for displaying info
 * in a ListView
 * @author Ras Fincher
 * @version 10/12/2018
 *
 */
public class PatientLite implements Comparable<PatientLite> {
	private String firstName;
	private String lastName;
	private int ssn;
	private int patientID;
	
	/**
	 * Constructor for a PatientLite object
	 * 
	 * @param firstName The first name
	 * @param lastName The last name
	 * @param patientID the patientID of the PatientLite
	 * 
	 * @postcondition A new PatientLite is created with it's fields initialized
	 */
	public PatientLite(String firstName, String lastName, int ssn, int patientID) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
		this.patientID = patientID;
	}
	
	/**
	 * Returns the first name
	 * 
	 * @return the first name
	 */
	public String getFirstName() {
		return this.firstName;
	}
	
	/**
	 * Returns the last name
	 * 
	 * @return the last name
	 */
	public String getLastName() {
		return this.lastName;
	}
	
	/**
	 * Returns the patientID
	 * 
	 * @return the patientID
	 */
	public int getPatientID() {
		return this.patientID;
	}
	
	/**
	 * Returns the patient's ssn
	 * 
	 * @return the patient's ssn
	 */
	public int getSsn() {
		return this.ssn;
	}
	
	/**
	 * Returns the PatientLite's full name
	 * 
	 * @return the PatientLite's full name
	 */
	public String getFullName() {
		return getFirstName() + " " + getLastName();
	}
	
	/**
	 * Returns the PatientLite's full name
	 * 
	 * @return the PatientLite's full name
	 */
	@Override
	public String toString() {
		return getFirstName() + " " + getLastName() + " - SSN: " + getSsn();
	}

	@Override
	public int compareTo(PatientLite o) {
		int comparison = this.getLastName().compareTo(o.getLastName());
		if (comparison == 0) {
			return this.getFirstName().compareTo(o.getFirstName());
		}
		return comparison;
	}
}
