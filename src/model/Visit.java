package model;

/**
 * Datastructure for carrying visit information.
 * @author David Jarrett
 * @version Fall 2018
 */
public class Visit {

	private int appointmentID;
	private int patientID;
	private int nurseID;
	private int systolic;
	private int diastolic;
	private double temp;
	private int pulse;
	private double weight;
	private String initialDiagnosis;
	private String finalDiagnosis;

	public Visit(int appointmentID, int patientID, int nurseID, int systolic, int diastolic, double temp, int pulse,
			double weight, String initialDiagnosis, String finalDiagnosis) {
		this.appointmentID = appointmentID;
		this.patientID = patientID;
		this.nurseID = nurseID;
		this.systolic = systolic;
		this.diastolic = diastolic;
		this.temp = temp;
		this.pulse = pulse;
		this.weight = weight;
		this.initialDiagnosis = initialDiagnosis;
		this.finalDiagnosis = finalDiagnosis;
	}

	public int getAppointmentID() {
		return this.appointmentID;
	}

	public void setAppointmentID(int appointmentID) {
		this.appointmentID = appointmentID;
	}

	public int getPatientID() {
		return this.patientID;
	}

	public void setPatientID(int patientID) {
		this.patientID = patientID;
	}

	public int getNurseID() {
		return this.nurseID;
	}

	public void setNurseID(int nurseID) {
		this.nurseID = nurseID;
	}

	public int getSystolic() {
		return this.systolic;
	}

	public void setSystolic(int systolic) {
		this.systolic = systolic;
	}

	public int getDiastolic() {
		return this.diastolic;
	}

	public void setDiastolic(int diastolic) {
		this.diastolic = diastolic;
	}

	public double getTemp() {
		return this.temp;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}

	public int getPulse() {
		return this.pulse;
	}

	public void setPulse(int pulse) {
		this.pulse = pulse;
	}

	public double getWeight() {
		return this.weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getInitialDiagnosis() {
		return this.initialDiagnosis;
	}

	public void setInitialDiagnosis(String initialDiagnosis) {
		this.initialDiagnosis = initialDiagnosis;
	}

	public String getFinalDiagnosis() {
		return this.finalDiagnosis;
	}

	public void setFinalDiagnosis(String finalDiagnosis) {
		this.finalDiagnosis = finalDiagnosis;
	}

}
