package model;

import java.sql.Date;

/**
 * Holds information related to a test.
 * @author David Jarrett
 * @version Fall 2018
 */
public class Test {

	private int testID;
	private int visitID;
	private String testName;
	private String testResults;
	private Date testDate;
	
	/**
	 * Initializes a Test with the tests ID and name.
	 * @preconditions: none
	 * @param testID
	 * @param testName
	 */
	public Test(int testID, String testName) {
		this.testID = testID;
		this.testName = testName;
	}

	public int getTestID() {
		return this.testID;
	}

	public String getTestName() {
		return this.testName;
	}
	
	public void setTestResults(String results) {
		this.testResults = results;
	}
	
	public String getTestResults() {
		return this.testResults;
	}
	
	public int getVisitID() {
		return this.visitID;
	}

	public void setVisitID(int visitID) {
		this.visitID = visitID;
	}

	@Override
	public String toString() {
		return this.testName;
	}
	
	public void setTestDate(Date date) {
		this.testDate = date;
	}
	
	public Date getTestDate() {
		return this.testDate;
	}
	
}
