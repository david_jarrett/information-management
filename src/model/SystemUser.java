package model;

import java.time.LocalDate;
import java.util.Objects;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * This class represents a user of the system (e.g., an employee using the system, not a patient).
 * @author David Jarrett
 * @version 09/17/2018
 */
public class SystemUser {
	
	private String userID;
	private String firstName;
	private String lastName;
	private LocalDate bDate;
	private String phone;
	
	private ObjectProperty<String> informationString = new SimpleObjectProperty<>(this, "informationString", null);
	
	/**
	 * Initializes the object with basic identifying information.
	 * @preconditions (firstName && lastName && userID) != null
	 * @postconditions The object will be initialized with basic identifying information.
	 * 
	 * @param firstName The user's first name.
	 * @param lastName The user's last name.
	 * @param userID The user's ID.
	 */
	public SystemUser(String firstName, String lastName, String userID) {
		this.firstName = Objects.requireNonNull(firstName, "firstName cannot be null");
		this.lastName = Objects.requireNonNull(lastName, "lastName cannot be null");
		this.userID = Objects.requireNonNull(userID, "userID cannot be null");
		
		this.informationString.set(String.join(", ", "Logged in: " + this.lastName, this.firstName, "ID: " + this.userID));
	}
	
	public SystemUser(String userID, String firstName, String lastName, LocalDate bDate, String phone) {
		this.userID = Objects.requireNonNull(userID, "userID cannot be null");
		this.firstName = Objects.requireNonNull(firstName, "firstName cannot be null");
		this.lastName = Objects.requireNonNull(lastName, "lastName cannot be null");
		this.bDate = Objects.requireNonNull(bDate, "bDate cannot be null");
		this.phone = Objects.requireNonNull(phone, "phone cannot be null");
		
		this.informationString.set(String.join(", ", "Logged in: " + this.lastName, this.firstName, "ID: " + this.userID));
	}
	
	/**
	 * Returns a string of the format \"Logged in: LastName, FirstName, ID: NNNNNNN"
	 * known as the information string.
	 * @preconditions None
	 * 
	 * @return The information string.
	 */
	public final String getInformationString() {
		return this.informationString.get();
	}
	
	/**
	 * Returns the information string property. See getInformationString().
	 * @preconditions None
	 * 
	 * @return The information string property.
	 */
	public ObjectProperty<String> getInformationStringProperty() {
		return this.informationString;
	}
	
	/**
	 * Returns the full name of this SystemUser
	 * 
	 * @return the full name of this SystemUser
	 */
	public String getFullName() {
		return firstName + " " + lastName;
	}
	
	public String getUserID() {
		return userID;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public LocalDate getbDate() {
		return bDate;
	}

	public String getPhone() {
		return phone;
	}
	
}
