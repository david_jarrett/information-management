package model;

import java.time.LocalDate;
import java.util.Objects;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This subclass of User represents a patient of the office.
 * @author David Jarrett
 * @version 09/15/2018
 */
public class Patient extends Person {

	private StringProperty patientID = new SimpleStringProperty(this, "patientID", "");
	
	/**
	 * Initializes the Patient object.
	 * @preconditions patientID != null
	 * 
	 * @param patientID The patient ID.
	 */
	public Patient(String firstName, String lastName, LocalDate dob, Address address, String phone, String gender, String ssn) {
		super(firstName, lastName, dob, address, phone, gender, ssn);
	}
	
	/**
	 * Returns the patient ID.
	 * @preconditions None
	 * 
	 * @return The patient ID.
	 */
	public final String getPatientID() {
		return this.patientID.get();
	}
	
	/**
	 * Sets the patient ID.
	 * @preconditions patientID != null
	 * @postconditions this.patientID == patientID
	 * 
	 * @param patientID The patient ID
	 */
	public final void setPatientID(String patientID) {
		this.patientID.set(Objects.requireNonNull(patientID, "patientID cannot be null"));
	}
	
	/**
	 * Returns the property that contains the patient ID.
	 * @preconditions None
	 * 
	 * @return The patientID property.
	 */
	public StringProperty getPatientIDProperty() {
		return this.patientID;
	}
	
	/**
	 * Sets the patient ID property
	 * 
	 * @param patientID the patientID being set
	 * 
	 * @postcondition this.patientID is set to patientID
	 */
	public void setPatientIDProperty(int patientID) {
		this.patientID.set(Integer.toString(patientID));
	}
}
