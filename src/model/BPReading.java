package model;

public class BPReading {

	private int systolic;
	private int diastolic;
	
	public BPReading(int systolic, int diastolic)
	{
		this.systolic = systolic;
		this.diastolic = diastolic;
	}
	
	public int getSystolic() {
		return this.systolic;
	}
	
	public int getDiastolic() {
		return this.diastolic;
	}
	
	public String getStatus() {
		int result = 0;
		if (this.systolic >= 120 && this.systolic < 130) {
			result++;
		} else if (this.systolic >= 130) {
			result += 2;
		}
		if (this.diastolic >= 80) {
			result += 2;
		}
		
		if (result == 0) {
			return "Normal";
		}
		if (result == 1) {
			return "Elevated";
		}
		return "High";
	}
	
}
