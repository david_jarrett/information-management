package model;

import dal.LoginDAL;
import dal.MySqlLoginDAL;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Manages the user accounts on the system.
 * @author David Jarrett
 * @version 10/11/2018
 */
public class LoginManager {
	
	private static LoginManager instance = null;

	private LoginDAL loginDAL = new MySqlLoginDAL();
	private ObjectProperty<SystemUser> currentUser = new SimpleObjectProperty<>(this, "currentUser", null);
		
	public static LoginManager getInstance() {
		if (instance != null) {
			return instance;
		} else {
			instance = new LoginManager();
			return instance;
		}
	}
	
	/**
	 * Ensures that the username/password combination provided is valid and logs the
	 * corresponding user into the system.
	 * 
	 * @param username The username.
	 * @param password The password.
	 * @return true if the username and password combination are valid, false if not.
	 */
	public boolean logUserIn(String username, String password) {
		if(this.loginDAL.verifyUser(username, password)) {
			this.setCurrentlyLoggedInUser();
			return true;
		}
		return false;
	}
	
	private void setCurrentlyLoggedInUser() {
		String firstName = this.loginDAL.getUserFirstName();
		String lastName = this.loginDAL.getUserLastName();
		String userID = this.loginDAL.getUserID();
		this.currentUser.set(new SystemUser(firstName, lastName, userID));
	}

	/**
	 * Returns the currently logged in user.
	 * @preconditions None
	 * 
	 * @return The currently logged in user.
	 */
	public final SystemUser getCurrentUser() {
		return this.currentUser.get();
	}
	
	/**
	 * Returns the current user property.
	 * @preconditions None
	 * 
	 * @return The current user property.
	 */
	public ObjectProperty<SystemUser> getCurrentUserProperty() {
		return this.currentUser;
	}

}
