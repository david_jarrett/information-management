package model;

import java.util.Objects;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Objects of this class store the information contained in an address.
 * @author David Jarrett
 * @version 09/15/2018
 */
public class Address {

	private StringProperty street = new SimpleStringProperty(this, "street", "");
	private StringProperty city = new SimpleStringProperty(this, "city", "");
	private ObjectProperty<State> state = new SimpleObjectProperty<>(this, "state", null);
	private StringProperty zip = new SimpleStringProperty(this, "zip", "");
	
	/**
	 * Initializes an Address with the specified data.
	 * @preconditions (street && city && state && zip) != null
	 * @postconditions this.street == street, this.city == city, this.state = ENUM_OF(state), this.zip == zip
	 * 
	 * @param street The street address.
	 * @param city The city of residence.
	 * @param state The state of residence.
	 * 				The state is passed in as a String, but converted internally to an Enum.
	 * @param zip The zipcode of residence.
	 */
	public Address(String street, String city, String state, String zip) {
		this.setStreet(street);
		this.setCity(city);
		this.setZip(zip);
		
		Objects.requireNonNull(state, "state cannot be null");
		this.setState(Enum.valueOf(State.class, state));
	}
	
	/**
	 * Returns the street address.
	 * @preconditions None
	 * 
	 * @return The street address.
	 */
	public final String getStreet() {
		return this.street.get();
	}
	
	/**
	 * Sets the street address.
	 * @preconditions street != null
	 * @postconditions this.street = street
	 */
	public final void setStreet(String street) {
		this.street.set(Objects.requireNonNull(street, "street cannot be null"));
	}
	
	/**
	 * Returns the property containing the street of residence.
	 * @preconditions None
	 * 
	 * @return The street of residence property.
	 */
	public StringProperty getStreetProperty() {
		return this.street;
	}
	
	/**
	 * Returns the city of residence.
	 * @preconditions None
	 * 
	 * @return The city of residence.
	 */
	public final String getCity() {
		return this.city.get();
	}
	
	/**
	 * Sets the city of residence.
	 * @preconditions city != null
	 * @postconditions this.city == city
	 */	
	public final void setCity(String city) {
		this.city.set(Objects.requireNonNull(city, "city cannot be null"));
	}
	
	/**
	 * Returns the property containing the city of residence.
	 * @preconditions None
	 * 
	 * @return The city of residence property.
	 */
	public StringProperty getCityProperty() {
		return this.city;
	}
	
	/**
	 * Returns the state of residence.
	 * @preconditions None
	 * 
	 * @return The state of residence.
	 */
	public final State getState() {
		return this.state.get();
	}
	
	/**
	 * Sets the state of residence.
	 * @preconditions state != null
	 * @postconditions this.state = state
	 */
	public final void setState(State state) {
		this.state.set(Objects.requireNonNull(state, "state cannot be null"));
	}
	
	/**
	 * Returns the property containing the state of residence.
	 * @preconditions None
	 * 
	 * @return The state of residence property.
	 */
	public ObjectProperty<State> getStateProperty() {
		return this.state;
	}
	
	/**
	 * Returns the zipcode of residence.
	 * @preconditions None
	 * 
	 * @return The zipcode of residence.
	 */
	public final String getZip() {
		return this.zip.get();
	}
	
	/**
	 * Sets the zipcode of residence.
	 * @preconditions zip != null
	 * @postconditions this.zip == zip
	 */
	public final void setZip(String zip) {
		this.zip.set(Objects.requireNonNull(zip, "zip cannot be null"));
	}
	
	/**
	 * Returns the property containing the zipcode of residence.
	 * @preconditions None
	 * 
	 * @return The zipcode of residence property.
	 */
	public StringProperty getZipProperty() {
		return this.zip;
	}
	
}
