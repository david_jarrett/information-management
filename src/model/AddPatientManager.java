package model;

import java.sql.SQLException;
import java.time.LocalDate;

import controller.PatientMaintenanceWindowController;
import dal.MySqlPatientDAL;
import dal.PatientDAL;

/**
 * Describes an AddPatientManager object
 * 
 * @author Ras Fincher
 * 
 * @version 10/11/2018
 *
 */
public class AddPatientManager {
	private PatientMaintenanceWindowController controller;
	
	/**
	 * Constructor for a AddPatientManager object
	 * 
	 * @param controller the window controller
	 * 
	 * @postcondition a new AddPatientManager is created with it's fields 
	 * initialized
	 */
	public AddPatientManager(PatientMaintenanceWindowController controller) {
		this.controller = controller;
	}
	
	/**
	 * Adds a Patient to the Datamodel
	 * 
	 * @postcondition a Patient is added to the datamodel
	 * 
	 * @throws SQLException if the Patient cannot be added
	 */
	public void addPatientToDataModel(boolean isEdit) throws SQLException {
		String firstName = this.controller.getFirstNameText();
    	String lastName = this.controller.getLastNameText();
    	String gender = this.controller.getGender();
    	LocalDate dob = this.controller.getDOB();
    	String ssn = this.controller.getSSN().replaceAll("[^\\d.]", "");
    	String phone = this.controller.getPhone();
    	String street = this.controller.getStreet();
    	String city = this.controller.getCity();
    	State state = this.controller.getState();
    	String zip = this.controller.getZip();
	    	
    	Address address = new Address(street, city, state.name(), zip);	    	
    	Patient newPatient = new Patient(firstName, lastName, dob, address, phone, gender, ssn);
    	
    	PatientDAL patientDAL = new MySqlPatientDAL();
    	if (isEdit) {
    		patientDAL.updatePatient(newPatient);
    	}
    	else {
    		patientDAL.addPatient(newPatient);
    	}
    	
    	PatientLite patientLite = patientDAL.getPatientLite(newPatient);
    	if (!isEdit) {
    		this.controller.getDataModel().getPatientList().add(patientLite);
    		this.controller.getDataModel().setCurrentPerson(patientLite);
    	} else {
    		this.controller.getDataModel().setCurrentPerson(null);
    		this.controller.getDataModel().setCurrentPerson(patientLite);
    	}
	}
}
