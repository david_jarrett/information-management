package model;

import java.util.HashMap;
import java.util.Map;

import dal.MySqlDoctorDAL;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This class represents the model-layer for the primary view. 
 * It is a Singleton that is shared among all of the controllers.
 * @author David Jarrett
 * @version 09/15/2018
 */
public class DataModel {
	
	private static DataModel instance;
	
	private final ObservableList<PatientLite> patientList = FXCollections.observableArrayList();
	
	private final ObjectProperty<PatientLite> selectedPatient = new SimpleObjectProperty<>(this, "selectedPatient", null); 
	
	private final ObservableList<String> doctorsList = FXCollections.observableArrayList();
	
	private Map<String, SystemUser> doctors = new HashMap<>();
	
	private int selectedAppointmentID;
	
	private DataModel() {
		this.loadDoctors();
	}
	
	/**
	 * The static instance-retrieval method for the DataModel. If an instance does not exist, one
	 * is created. If the instance already exists, then the current instance is returned.
	 * @preconditions None
	 * 
	 * @return The singleton instance of the DataModel.
	 */
	public static DataModel getInstance() {
		if (instance == null) {
			instance = new DataModel();
		}
		return instance;
	}
	
	/**
	 * Returns the currently selected person. The concept of a currently selected person is central
	 * in this application. Any controller accessing the data model from any point in the application
	 * will be aware of the currently selected person.
	 * @preconditions None
	 * 
	 * @return The currently selected person (or null if no person is selected).
	 */
	public final PatientLite getCurrentPerson() {
		return this.selectedPatient.get();
	}
	
	/**
	 * Sets the currently selected person. The currently selected person can be null, which
	 * means that there is no currently selected person.
	 * @preconditions None (Nullable)
	 * @postconditions this.currentPerson == currentPerson
	 * 
	 * @param currentPerson The newly selected currentPerson.
	 */
	public final void setCurrentPerson(PatientLite currentPerson) {
		this.selectedPatient.set(currentPerson);
	}
	
	/**
	 * Sets the current person
	 * 
	 * @param id the id of the person being set
	 */
	public final void setCurrentPerson(int id) {
		for (var patient : this.patientList) {
			if (patient.getPatientID() == id) {
				this.setCurrentPerson(patient);
				break;
			}
		}
	}
	
	/**
	 * Returns the property that contains the currently selected person. Bind to this property in order to
	 * observe the currently selected person in the application.
	 * @preconditions None
	 * 
	 * @return The currentPerson property.
	 */
	public ObjectProperty<PatientLite> getCurrentPersonProperty() {
		return this.selectedPatient;
	}
	
	/**
	 * Returns the client list. The client list is a collection of clients that are represented in the data model.
	 * @preconditions None
	 * 
	 * @return The client list.
	 */
	public ObservableList<PatientLite> getPatientList() {
		return this.patientList;
	}
	
	/**
	 * Returns an observable list of all the doctors
	 * 
	 * @return an observable list of all the doctors
	 */
	public ObservableList<String> getDoctors() {
		return this.doctorsList;
	}
	
	/**
	 * Returns a map from Doctor's name to the associated SystemUser 
	 * 
	 * @return a map from Doctor's name to the associated SystemUser 
	 */
	public Map<String, SystemUser> getDoctorMap() {
		return doctors;
	}
	
	private final void loadDoctors() {
		doctors = new MySqlDoctorDAL().getDoctors();
		for (String s : doctors.keySet()) {
			this.doctorsList.add(s);
		}
	}
	
	/**
	 * Sets the selected appointment ID based on the visit ID
	 * 
	 * @param visitID the visit ID
	 */
	public void setSelectedAppointmentID(int visitID) {
		this.selectedAppointmentID = visitID;
	}
	
	/**
	 * Gets the selected appointment ID
	 * 
	 * @return the selected appointment ID
	 */
	public int getSelectedAppointmentID() {
		return this.selectedAppointmentID;
	}
}
