package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Test;

public class MySqlTestDAL implements TestDAL {

	public List<Test> getTests() {
		String query = "SELECT * FROM `Tests`;";
		List<Test> tests = new ArrayList<>();
		
		try (var con = DbConnection.getConnection()) {
			Statement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				int testID = rs.getInt("testID");
				String testName = rs.getString("testName");
				Test aTest = new Test(testID, testName);
				tests.add(aTest);
			}
			return tests;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tests;
	}
	
	public List<Test> getTestsFor(int appointmentID) {
		List<Test> tests = new ArrayList<>();
		List<Integer> testIds = new ArrayList<>();
		Map<Integer, Date> testDates = new HashMap<>();
		
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT visitID FROM `Visit` WHERE apptID=?;";
			PreparedStatement qryStmt = con.prepareStatement(query);
			qryStmt.setInt(1, appointmentID);
			var rs = qryStmt.executeQuery();
			
			int visitID = 0;
			if (rs.next()) {
				visitID = rs.getInt(1);
			}
			
			query = "SELECT * FROM `OrderedTests` WHERE visitID=?;";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, visitID);
			rs = stmt.executeQuery();
			while (rs.next()) {
				int testID = rs.getInt("testID");
				Date testDate = rs.getDate("resultsDate");
				testIds.add(testID);
				testDates.put(testID, testDate);
			}
			
			query = "SELECT * FROM `Tests` WHERE testID=?;";
			for (int testID : testIds) {
				stmt = con.prepareStatement(query);
				stmt.setInt(1, testID);
				rs = stmt.executeQuery();
				while (rs.next()) {
					int id = rs.getInt("testID");
					String testName = rs.getString("testName");
					var test = new Test(id, testName);
					test.setVisitID(visitID);
					test.setTestDate(testDates.get(id));
					tests.add(test);
				}
			}
			
			return tests;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tests;
	}

	public void orderTestFor(int appointmentID, Test test) {
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT visitID FROM `Visit` WHERE apptID=?;";
			PreparedStatement qryStmt = con.prepareStatement(query);
			qryStmt.setInt(1, appointmentID);
			var rs = qryStmt.executeQuery();
			
			int visitID = 0;
			if (rs.next()) {
				visitID = rs.getInt(1);
			}
			
			String insert = "INSERT INTO `OrderedTests`(testID, visitID) VALUES (?, ?);";
			PreparedStatement stmt = con.prepareStatement(insert);
			stmt.setInt(1, test.getTestID());
			stmt.setInt(2, visitID);			
			stmt.executeUpdate();
		} catch (SQLException e) {
			//ignoring duplicate selections should be fine
		}
	}
	
	public String getTestResultsFor(int testID, int visitID) {
		String result = null;
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT testResults FROM `OrderedTests` WHERE testID=? AND visitID=?;";
			PreparedStatement qryStmt = con.prepareStatement(query);
			qryStmt.setInt(1, testID);
			qryStmt.setInt(2, visitID);
			var rs = qryStmt.executeQuery();
			
			if (rs.next()) {
				result = rs.getString("testResults");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public void updateResultsFor(Test currentTest) {
		try (var con = DbConnection.getConnection()) {
			String update = "UPDATE `OrderedTests` SET testResults=?, resultsDate=? WHERE testID=? AND visitID=?;";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setString(1, currentTest.getTestResults());
			stmt.setDate(2, currentTest.getTestDate());	
			stmt.setInt(3, currentTest.getTestID());
			stmt.setInt(4, currentTest.getVisitID());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String getTestResultDateFor(int testID, int visitID) {
		String result = null;
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT resultsDate FROM `OrderedTests` WHERE testID=? AND visitID=?;";
			PreparedStatement qryStmt = con.prepareStatement(query);
			qryStmt.setInt(1, testID);
			qryStmt.setInt(2, visitID);
			var rs = qryStmt.executeQuery();
			
			if (rs.next()) {
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				var date = rs.getDate(1);
				if (date != null) {
					result = df.format(date);
				} else {
					result = "";
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}
