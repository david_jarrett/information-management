package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Address;
import model.BPReading;
import model.Patient;
import model.PatientLite;

public class MySqlPatientDAL implements PatientDAL{
	
	/**
	 * Adds a client's address to the Address table of the database
	 * 
	 * @param address 	the address being added
	 *
	 * @throws SQLException if the Patient cannot be added
	 */
	public void addPatient(Patient newPatient) throws SQLException {
			var con = DbConnection.getConnection();
			String fname = newPatient.getFirstName();
			String lname = newPatient.getLastName();
			String gender = newPatient.getGender();
			String ssn = newPatient.getSSN();
			String phone = newPatient.getPhone();
			
			String insert = "INSERT INTO `Patient` " 
					+ "(fname, lname, bdate, gender, ssn, phone) "
					+ "VALUES (?, ?, ?, ?, ?, ?);"
					+ "INSERT INTO Address (street, city, state, zip, addressID) VALUES (?, ?, ?, ?, LAST_INSERT_ID())";
			
			PreparedStatement stmt = con.prepareStatement(insert);
			stmt.setString(1, fname);
			stmt.setString(2, lname);
			stmt.setTimestamp(3, Timestamp.valueOf(newPatient.getDateOfBirth().atTime(12, 00)));
			stmt.setString(4, gender);
			stmt.setString(5, ssn);
			stmt.setString(6, phone);
			
			var address = newPatient.getAddress();
			
			stmt.setString(7, address.getStreet());
			stmt.setString(8, address.getCity());
			stmt.setString(9, address.getState().name());
			stmt.setString(10, address.getZip());			
			stmt.executeUpdate();
			con.close();
	}
	
	public void removePatient(Patient patient) throws SQLException {
		var con = DbConnection.getConnection();
		var ssn = patient.getSSN();
		
		String remove = "DELETE FROM `Patient` WHERE ssn=?;"; 
		PreparedStatement stmt = con.prepareStatement(remove);
		stmt.setString(1, ssn);
		stmt.executeUpdate();
		con.close();
	}
	
	public void updatePatient(Patient patient) throws SQLException {
		var con = DbConnection.getConnection();
				
		String query = "select patientID from `Patient` WHERE ssn=?;";
		
		PreparedStatement ps = con.prepareStatement(query);
		ps.setString(1, patient.getSSN());
		ResultSet rs = ps.executeQuery();
		
		String patientID = null;
		if (rs.next()) {
			patientID = rs.getString(1);
		}
		
		String update = "UPDATE `Patient` " 
				+ "set fname=?, lname=?, bdate=?, gender=?, ssn=?, phone=? "
				+ "WHERE patientID=?;"
				+ "UPDATE `Address` set street=?, city=?, state=?, zip=? "
				+ "WHERE addressID=?;"; 
		
		PreparedStatement stmt = con.prepareStatement(update);
		stmt.setString(1, patient.getFirstName());
		stmt.setString(2, patient.getLastName());
		stmt.setTimestamp(3, Timestamp.valueOf(patient.getDateOfBirth().atTime(12, 00)));
		stmt.setString(4, patient.getGender());
		stmt.setString(5, patient.getSSN());
		stmt.setString(6, patient.getPhone());
		stmt.setString(7, patientID);
		
		stmt.setString(8, patient.getAddress().getStreet());
		stmt.setString(9, patient.getAddress().getCity());
		stmt.setString(10, patient.getAddress().getState().name());
		stmt.setString(11, patient.getAddress().getZip());
		stmt.setString(12, patientID);
		stmt.executeUpdate();
		con.close();
	}
	
	/**
	 * Returns a Map that maps clientIDs to full name of a client for a ListView
	 * 
	 * @return a Map that maps clientIDs to full name of a client for a ListView
	 */
	@Override
	public List<PatientLite> getPatientNames() {
		List<PatientLite> clients = new ArrayList<PatientLite>();
		
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT patientID, fname, lname FROM Patient";
			Statement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				String firstName = rs.getString("fname");
				String lastName = rs.getString("lname");
				int ssn = rs.getInt("ssn");
				int clientID = rs.getInt("patientID");
				clients.add(new PatientLite(firstName, lastName, ssn, clientID));
			}
			return clients;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clients;
	}
	
	/**
	 * Returns a PatientLite representation of a Patient object
	 * 
	 * @param Patient the Patient being requested
	 * 
	 * @return a PatientLite representation of a Patient object
	 */
	@Override
	public PatientLite getPatientLite(Patient client) {
		PatientLite clientLite = null;
		
		try (var con = DbConnection.getConnection()) {
			String query = "select fname, lname, patientID from `Patient` WHERE ssn=?;";
			
			PreparedStatement ps = con.prepareStatement(query);
			String ssn = client.getSSN();
			ps.setString(1, ssn);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				String firstName = rs.getString(1);
				String lastName = rs.getString(2);
				int clientID = rs.getInt(3);
				clientLite = new PatientLite(firstName, lastName, Integer.parseInt(ssn), 
						clientID);
			}
			return clientLite;
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return clientLite;
	}
	
	/**
	 * Gets the Patient for an associated clientID from the Database
	 * 
	 * @preconditions clientID >= 0
	 * 
	 * @param clientID	the desired clientID
	 * 
	 * @return the Patient for an associated clientID from the Database
	 */
	@Override
	public Patient getDataForPatient(int clientID) {
		Patient client = null;
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT p.fname, p.lname, p.bdate, p.ssn, p.gender, p.phone, a.street, a.city, a.state, a.zip"
			+ " FROM `Address` a, `Patient` p"
			+ " WHERE a.addressID = p.patientID AND p.patientID = ?";
			
			PreparedStatement pStmt = con.prepareStatement(query);
			pStmt.setInt(1, clientID);
			ResultSet rs = pStmt.executeQuery();
			
			while (rs.next()) {
				String firstName = rs.getString("fname");
				String lastName = rs.getString("lname");
				LocalDate dob = rs.getDate("bdate").toLocalDate();
				String ssn = rs.getString("ssn");
				Address address = new Address(rs.getString("street"), rs.getString("city"), 
						rs.getString("state"), rs.getString("zip"));
				String gender = rs.getString("gender");
				String phone = rs.getString("phone");
				
				client = new Patient(firstName, lastName, dob, address, phone, gender, ssn);
			}
			return client;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return client;
	}
	
	public List<BPReading> getRecentBPHistory(int patientID) {
		var result = new ArrayList<BPReading>();
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT visitID, sysBP, diaBP"
			+ " FROM Visit"
			+ " WHERE patientID=?"
			+ " ORDER BY visitID DESC"
			+ " LIMIT 3;";
			
			PreparedStatement pStmt = con.prepareStatement(query);
			pStmt.setInt(1, patientID);
			ResultSet rs = pStmt.executeQuery();
			
			while (rs.next()) {
				int systolic = rs.getInt("sysBP");
				int diastolic = rs.getInt("diaBP");
				result.add(new BPReading(systolic, diastolic));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Map <String, Timestamp> getPatientAppointments(int patientID) {
		Map <String, Timestamp> result = new HashMap<>();
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT a.`date`, a.`apptID`"
					+ " FROM `Appointment` a"
					+ " WHERE a.`patientID` = ?;";
			
			PreparedStatement p = con.prepareStatement(query);
			p.setInt(1, patientID);
			
			ResultSet r = p.executeQuery();
			
			while (r.next()) {
				result.put("<" + r.getString(2) + ">" + r.getTimestamp(1).toString(), r.getTimestamp(1));
			}
			
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;	
	}
	
	public List<PatientLite> getPatients(String firstName, String lastName) {
		List<PatientLite> patients = new ArrayList<>();
		
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT `fname`, `lname`, `patientID`, `ssn`"
						+ " FROM `Patient`"
						+ " WHERE `fname` = ? AND `lname` = ?;";
			
			PreparedStatement p = con.prepareStatement(query);
			p.setString(1, firstName);
			p.setString(2, lastName);
			
			ResultSet r = p.executeQuery();
			
			while (r.next()) {
				String fName = r.getString("fname");
				String lName = r.getString("lname");
				int ssn = r.getInt("ssn");
				int pID = r.getInt("patientID");
				
				PatientLite pLite = new PatientLite(fName, lName, ssn, pID);
				patients.add(pLite);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return patients;
	}
	
	public List<PatientLite> getPatients(String dob) {
		List<PatientLite> patients = new ArrayList<>();
		
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT `fname`, `lname`, `patientID`, `ssn`"
						+ " FROM `Patient`"
						+ " WHERE `bdate` = ?;";
			
			PreparedStatement p = con.prepareStatement(query);
			p.setString(1, dob);
			
			ResultSet r = p.executeQuery();
			
			while (r.next()) {
				String fName = r.getString("fname");
				String lName = r.getString("lname");
				int ssn = r.getInt("ssn");
				int pID = r.getInt("patientID");
				
				PatientLite pLite = new PatientLite(fName, lName, ssn, pID);
				patients.add(pLite);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return patients;
	}
	
	public List<PatientLite> getPatients(String firstName, String lastName, String dob) {
		List<PatientLite> patients = new ArrayList<>();
		
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT `fname`, `lname`, `patientID`, `ssn`"
						+ " FROM `Patient`"
						+ " WHERE `fname` = ? AND `lname` = ? AND `bdate` = ?;";
			
			PreparedStatement p = con.prepareStatement(query);
			p.setString(1, firstName);
			p.setString(2, lastName);
			p.setString(3, dob);
			
			ResultSet r = p.executeQuery();
			
			while (r.next()) {
				String fName = r.getString("fname");
				String lName = r.getString("lname");
				int ssn = r.getInt("ssn");
				int pID = r.getInt("patientID");
				
				PatientLite pLite = new PatientLite(fName, lName, ssn, pID);
				patients.add(pLite);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return patients;
	}
	
	
}