package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import model.SystemUser;

/**
 * Implementation of the SystemUserDAL interface for MySql.
 * @author Ras Fincher and David Jarrett
 * @version 10/11/2018
 */
public class MySqlSystemUserDAL implements SystemUserDAL {
	private static final int ADMIN = 1;
	private static final int DOCTOR = 2;
	private static final int NURSE = 3;
	
	@Override
	public boolean isAdmin(SystemUser currentUser) {
		String query = "SELECT `roleID` FROM `SystemUser` WHERE `userID`=?;";
		
		try (var con = DbConnection.getConnection()) {
			PreparedStatement s = con.prepareStatement(query);
			s.setInt(1, Integer.parseInt(currentUser.getUserID()));
			ResultSet rs = s.executeQuery();
			
			if (rs.next()) {
				return rs.getInt(1) == ADMIN;
			}
			
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean isDoctor(SystemUser currentUser) {
		String query = "SELECT `roleID` FROM `SystemUser` WHERE `userID`=?;";
		
		try (var con = DbConnection.getConnection()) {
			PreparedStatement s = con.prepareStatement(query);
			s.setInt(1, Integer.parseInt(currentUser.getUserID()));
			ResultSet rs = s.executeQuery();
			
			if (rs.next()) {
				return rs.getInt(1) == DOCTOR;
			}
			
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean isNurse(SystemUser currentUser) {
		String query = "SELECT `roleID` FROM `SystemUser` WHERE `userID`=?;";
		
		try (var con = DbConnection.getConnection()) {
			PreparedStatement s = con.prepareStatement(query);
			s.setInt(1, Integer.parseInt(currentUser.getUserID()));
			ResultSet rs = s.executeQuery();
			
			if (rs.next()) {
				return rs.getInt(1) == NURSE;
			}
			
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public String queryResult(String query) throws SQLException {
		var con = DbConnection.getConnection();
		StringBuilder sb = new StringBuilder();
		query = query.replaceAll("\"", "'");
		query = query.replaceAll("\n", " ");
		System.out.println(query);
		Statement s = con.createStatement();
		if (query.toLowerCase().startsWith("select")) {
			ResultSet rs = s.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numberOfColumns = rsmd.getColumnCount();
			
			
			for (int i = 0; i < numberOfColumns; i++) {
				sb.append(String.format("%1$-20s", rsmd.getColumnLabel(i + 1)));
			}
			sb.append(System.lineSeparator());
			while (rs.next()) {
				for (int i = 0; i < numberOfColumns; i++) {
					sb.append(String.format("%1$-20s", rs.getString(i + 1)));
				}
				sb.append(System.lineSeparator());
			}
		} else {
			s.executeUpdate(query);
			sb.append("Query was successfully executed.");
		}
		return sb.toString();
	}

	@Override
	public String getName(int userID) throws SQLException {
		String query = "SELECT CONCAT(`fname`, ' ', `lname`) FROM `SystemUser` WHERE `userID`=?;";
		String result = "";
		try (var con = DbConnection.getConnection()) {
			PreparedStatement s = con.prepareStatement(query);
			s.setInt(1, userID);
			ResultSet rs = s.executeQuery();
			
			if (rs.next()) {
				result = rs.getString(1);
			}
		}
		return result;
	}
}
