package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Implementation of AppointmentDAL interface for MySql
 * @author Ras Fincher and David Jarrett
 *
 */
public class MySqlAppointmentDAL implements AppointmentDAL {

	@Override
	public void addAppointment(int patientID, int nurseID, int doctorID, 
			Timestamp date, String reason) throws SQLException {
		
		var con = DbConnection.getConnection();
		String insert = "INSERT INTO `Appointment` " +
						"(patientID, createdBy, doctorID, date, reason) " +
						"VALUES (?, ?, ?, ?, ?);";
		PreparedStatement s = con.prepareStatement(insert);
		
		s.setInt(1, patientID);
		s.setInt(2, nurseID);
		s.setInt(3, doctorID);
		s.setTimestamp(4, date);
		s.setString(5, reason);
		
		s.executeUpdate();
	}

	@Override
	public int getAppointmentID(int patientID, Timestamp date) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "SELECT a.`apptID` FROM `Appointment` a WHERE a.`patientID` = ?"
				+ " AND a.`date` = ?;";
		
		PreparedStatement s = con.prepareStatement(query);
		
		s.setInt(1, patientID);
		s.setTimestamp(2, date);
		
		ResultSet rs = s.executeQuery();
		int result = -1;
		while (rs.next()) {
			result = rs.getInt(1);
		}
		return result;
	}
	
	@Override
	public String getDoctorName(int apptID) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "SELECT CONCAT(s.`fname`, ' ', s.`lname`)"
				+ " FROM `Appointment` a, `SystemUser` s"
				+ " WHERE a.`apptID` = ?"
				+ " AND a.`doctorID` = s.`userID`;";
		
		PreparedStatement s = con.prepareStatement(query);
		
		s.setInt(1, apptID);
		
		ResultSet rs = s.executeQuery();
		String result = "";
		while (rs.next()) {
			result = rs.getString(1);
		}
		return result;
	}
}
