package dal;

import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Interface for managing appointment data in a data source
 * @author Ras Fincher and David Jarrett
 *
 */
public interface AppointmentDAL {
	
	/**
	 * Adds an appointment to the data source
	 * @param patientID an int representing the patientID
	 * @param nurseID an int representing the nurseID
	 * @param doctorID an int representing the doctorID
	 * @param date a Timestamp representing the appointment time
	 * @param reason a String representing the reason for the appointment
	 * @throws SQLException
	 */
	void addAppointment(int patientID, int nurseID, int doctorID, Timestamp date, String reason) throws SQLException;
	
	/**
	 * Returns an appointmentID based on patientID and the date
	 * 
	 * @param patientID an int representing the patientID
	 * @param date a Timestamp representing the date
	 * @return the requested appointmentID
	 * @throws SQLException
	 */
	int getAppointmentID(int patientID, Timestamp date) throws SQLException;
	
	/**
	 * Returns the name of the doctor associated with an appointment
	 * 
	 * @param apptID the doctorID
	 * @return the name of the doctor associated with an appointment
	 * @throws SQLException
	 */
	String getDoctorName(int apptID) throws SQLException;
}
