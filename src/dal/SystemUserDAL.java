package dal;

import java.sql.SQLException;

import model.SystemUser;

/**
 * Interface for managing system user information
 * 
 * @author Ras Fincher and David Jarrett
 * @version 10/11/2018
 *
 */
public interface SystemUserDAL {
	
	/**
	 * Checks if the current SystemUser has the role of admin
	 * 
	 * @return true if the SystemUsers role is a admin, 
	 * 			false otherwise.
	 */
	boolean isAdmin(SystemUser currentUser);
	
	/**
	 * Checks if the current SystemUser has the role of doctor
	 * 
	 * @return true if the SystemUsers role is a doctor, 
	 * 			false otherwise.
	 */
	boolean isDoctor(SystemUser currentUser);
	
	/**
	 * Checks if the current SystemUser has the role of nurse
	 * 
	 * @return true if the SystemUsers role is a nurse, 
	 * 			false otherwise.
	 */
	boolean isNurse(SystemUser currentUser);
	
	String queryResult(String query) throws SQLException;
	
	/**
	 * Returns a string containing the SystemUsers full name
	 * 
	 * @param userID the userID
	 * @return a string containing the SystemUsers full name
	 * @throws SQLException
	 */
	String getName(int userID) throws SQLException;
}
