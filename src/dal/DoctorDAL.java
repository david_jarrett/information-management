package dal;

import java.util.Map;

import model.SystemUser;

/**
 * Interface for managing doctor data in a data source
 * @author Ras Fincher and David Jarrett
 *
 */
public interface DoctorDAL {
	
	/**
	 * Returns a map where the keys are doctors names
	 * and the values are the associated SystemUsers
	 * 
	 * @return a map where the keys are doctors names
	 * and the values are the associated SystemUsers
	 */
	Map<String, SystemUser> getDoctors();
}
