package dal;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

/**
 * Implementation of the LoginDAL interface for MySql.
 * @author David Jarrett
 * @version 10/11/2018
 */
public class MySqlLoginDAL implements LoginDAL {

	private static final String loginQuery = "select username, password, loginID, fname, lname from Login as log, SystemUser as su where log.loginID=su.userID and log.username=?";
	
	private String userFirstName;
	private String userLastName;
	private String userID;
	
	/**
	 * If the provided username/password combination is valid, the DAL will be loaded with user information.
	 * @preconditions: username != null && password != null
	 * @postconditions: firstName, lastName, and userID information will be available.
	 * @return true if user was logged-in successfully, false otherwise 
	 */
	@Override
	public boolean verifyUser(String username, String password) {
		try(var con = DbConnection.getConnection()) {
			var query = prepareQuery(username, password, con);
			var result = query.executeQuery();
			if (result.next() && this.verifyPassword(result, password)) {
				setUserInformation(result);
				return true;
			}
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	private boolean verifyPassword(ResultSet result, String password) throws SQLException {
		var hash = result.getBytes("password");
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-512");
			byte[] hashedPassword = md.digest(password.getBytes(StandardCharsets.UTF_8));
			return Arrays.equals(hash, hashedPassword);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return false;
	}

	private PreparedStatement prepareQuery(String username, String password, Connection con) throws SQLException {
		var query = con.prepareStatement(loginQuery);
		query.setString(1, username);
		//query.setString(2, password);
		return query;
	}
	
	private void setUserInformation(ResultSet result) throws SQLException {
		this.userFirstName = result.getString(4);
		this.userLastName = result.getString(5);
		this.userID = result.getString(3);
	}

	@Override
	public String getUserFirstName() {
		return this.userFirstName;
	}

	@Override
	public String getUserLastName() {
		return this.userLastName;
	}

	@Override
	public String getUserID() {
		return this.userID;
	}

	@Override
	public void setPassword(String username, String password) throws SQLException{
		var con = DbConnection.getConnection();
		String query = "UPDATE `Login` SET `password` = ? WHERE `username` = ?;";
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-512");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		byte[] hashedPassword = md.digest(password.getBytes(StandardCharsets.UTF_8));
		
		PreparedStatement p = con.prepareStatement(query);
		p.setBytes(1, hashedPassword);
		p.setString(2, username);
		
		p.executeUpdate();
	}
	


}
