package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Visit;

public class MySqlVisitDAL implements VisitDAL {
	
	@Override
	public void createVisit(int apptID, int patientID, int nurseID, int sysBP, int diaBP, double temp, int pulse,
			double weight, String initialDiagnosis, String finalDiagnosis) throws SQLException {
		var con = DbConnection.getConnection();

		String insert = "INSERT INTO `Visit` " + "(apptID, patientID, nurseID, sysBP, diaBP, temp, pulse, weight,"
				+ " initialDiagnosis, finalDiagnosis)" + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

		PreparedStatement stmt = con.prepareStatement(insert);
		stmt.setInt(1, apptID);
		stmt.setInt(2, patientID);
		stmt.setInt(3, nurseID);
		stmt.setInt(4, sysBP);
		stmt.setInt(5, diaBP);
		stmt.setDouble(6, temp);
		stmt.setInt(7, pulse);
		stmt.setDouble(8, weight);
		stmt.setString(9, initialDiagnosis);
		stmt.setString(10, finalDiagnosis);

		stmt.executeUpdate();
		con.close();

	}
	
	public void updateVisit(Visit visit) {
		String update = "UPDATE `Visit` SET"
				+ " nurseID=?, sysBP=?, diaBP=?, temp=?, pulse=?, weight=?, initialDiagnosis=?, finalDiagnosis=?"
				+ " WHERE apptID=?;";
		
		try (var con = DbConnection.getConnection()) {
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setInt(1, visit.getNurseID());
			stmt.setInt(2, visit.getSystolic());
			stmt.setInt(3, visit.getDiastolic());
			stmt.setDouble(4, visit.getTemp());
			stmt.setInt(5, visit.getPulse());
			stmt.setDouble(6, visit.getWeight());
			stmt.setString(7, visit.getInitialDiagnosis());
			stmt.setString(8, visit.getFinalDiagnosis());
			stmt.setInt(9, visit.getAppointmentID());
			
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public Visit getVisit(int appointmentID) throws NoVisitException {
		Visit visit = null;
		try (var con = DbConnection.getConnection()) {
			String query = "SELECT v.apptID, v.patientID, v.nurseID, v.sysBP, v.diaBP, v.temp, v.pulse, v.weight, v.initialDiagnosis, v.finalDiagnosis"
					+ " FROM `Visit` v" + " WHERE v.apptID=?";

			PreparedStatement pStmt = con.prepareStatement(query);
			pStmt.setInt(1, appointmentID);
			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				visit = new Visit(rs.getInt("apptID"), rs.getInt("patientID"), rs.getInt("nurseID"), rs.getInt("sysBP"),
						rs.getInt("diaBP"), rs.getDouble("temp"), rs.getInt("pulse"), rs.getDouble("weight"),
						rs.getString("initialDiagnosis"), rs.getString("finalDiagnosis"));
			} else {
				throw new NoVisitException();
			}
			return visit;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return visit;
	}
	
	public class NoVisitException extends Exception {
		private static final long serialVersionUID = 1L;
	}

}
