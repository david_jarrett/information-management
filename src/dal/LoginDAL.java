package dal;

import java.sql.SQLException;

/**
 * Interface for logging a user into a data source.
 * @author David Jarrett
 * @version 10/11/2018
 */
public interface LoginDAL {
	
	/**
	 * Ensures the the username/password combination is valid.
	 * 
	 * @preconditions: None
	 * @postconditions: getUserFirstName(), getUserLastName(), and getUserID() should be callable.
	 * @param username The username for the data source.
	 * @param password The password for the data source.
	 * @return true if the user was successfully logged in, false otherwise.
	 */
	boolean verifyUser(String username, String password);
	
	/**
	 * Returns the first name of the verified user.
	 * 
	 * @preconditions: The user should be logged in.
	 * @return The user's first name.
	 */
	String getUserFirstName();
	
	/**
	 * Returns the last name of the verified user.
	 * 
	 * @preconditions: The user should be logged in.
	 * @return The user's last name.
	 */
	String getUserLastName();
	
	/**
	 * Returns the userID of the verified user.
	 * @preconditions: The user should be logged in.
	 * @return The user's ID.
	 */
	String getUserID();
	
	/**
	 * Sets the hashed password of a user
	 * S
	 * @param username
	 * @param password
	 */
	void setPassword(String username, String password) throws SQLException;
}
