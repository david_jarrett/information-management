package dal;

import java.util.List;

import model.Test;

public interface TestDAL {
	/**
	 * Returns a list of all the tests
	 * 
	 * @return a list of all the tests
	 */
	List<Test> getTests();
	
	/**
	 * Returns a list of all the tests for a certain appointment
	 * 
	 * @param appointmentID the id of the appointment
	 * @return a list of all the tests for a certain appointment
	 */
	List<Test> getTestsFor(int appointmentID);
	
	/**
	 * Orders a test for a certain appointment
	 * 
	 * @param appointmentID the id of the appointment
	 * @param test the test being ordered
	 */
	void orderTestFor(int appointmentID, Test test);
	
	/**
	 * Returns the results for a given test
	 * 
	 * @param testID the id of the test
	 * @param visitID the id of the visit
	 * @return the results for a given test
	 */
	String getTestResultsFor(int testID, int visitID);
	
	/**
	 * Updates the results for the current test
	 * 
	 * @param currentTest the test being updated
	 */
	void updateResultsFor(Test currentTest);
	
	/**
	 * Gets the test result date for a test
	 * 
	 * @param testID the id of the test
	 * @param visitID the id of the visit
	 * @return the result date for a test
	 */
	String getTestResultDateFor(int testID, int visitID);
}
