package dal;


import java.sql.SQLException;
import java.util.List;

import model.Patient;
import model.PatientLite;

/**
 * Interface for managing patient data in a data source
 * @author Ras Fincher
 * @version 10/11/2018
 *
 */
public interface PatientDAL {
	
	/**
	 * Adds a patient to the data source
	 * 
	 * @param newPatient the patient being added
	 * @throws SQLException 
	 */
	void addPatient(Patient newPatient) throws SQLException;
	
	/**
	 * Removes a patient from the data source
	 * 
	 * @param patient the patient being removed
	 * @throws SQLException
	 */
	void removePatient(Patient patient) throws SQLException;
	
	/**
	 * Updates a patient on the data source
	 * 
	 * @param patient the patient being updated
	 * @throws SQLException
	 */
	void updatePatient(Patient patient) throws SQLException;
	
	/**
	 * Returns a list of PatientLites to use for displaying 
	 * names in a ListView
	 * 
	 * @return a list of PatientLites to use for displaying 
	 * names in a ListView
	 */
	List<PatientLite> getPatientNames();
	
	/**
	 * Returns the related Patient based on PatientID
	 * 
	 * @param patientID the patientID that corresponds
	 * to the desired Patient
	 * 
	 * @return the related Patient based on PatientID
	 */
	Patient getDataForPatient(int patientID);
	
	/**
	 * Returns a PatientLite representation of a Patient object
	 * 
	 * @param newPatient the Patient being requested
	 * 
	 * @return a PatientLite representation of a Patient object
	 */
	PatientLite getPatientLite(Patient newPatient);
}
