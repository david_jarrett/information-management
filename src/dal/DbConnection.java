package dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
	/**
	 * Gets a db Connection
	 * 
	 * @return a Connection object to the DB
	 */
	public static Connection getConnection() {
		String connectionString = "jdbc:mysql://160.10.25.16:3306/cs3230f18e?useSSL=false&user=cs3230f18e&password=ANbvqOESKngP1RzL&serverTimezone=EST&allowMultiQueries=true";
		Connection con = null;
		try {
			con = DriverManager.getConnection(connectionString); 
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		} catch (Exception e) {
            System.out.println(e.toString());
        }
		return con;
      }
}