package dal;

import java.sql.SQLException;

public interface VisitDAL {
	
	/**
	 * Creates a visit
	 * @param apptID the appointment id
	 * @param patientID the patient id
	 * @param nurseID the nurse id of the nurse assigned to the visit
	 * @param sysBP the systolic blood pressure reading
	 * @param diaBP the diastolic blood pressure reading
	 * @param temp the patient's temp
	 * @param pulse the patient's pulse
	 * @param weight the patient's weight
	 * @param initialDiagnosis the initial diagnosis
	 * @param finalDiagnosis the final diagnosis
	 * @throws SQLException
	 */
	void createVisit(int apptID, int patientID, int nurseID, int sysBP, int diaBP, double temp, int pulse, double weight, String initialDiagnosis, String finalDiagnosis) throws SQLException;
}
