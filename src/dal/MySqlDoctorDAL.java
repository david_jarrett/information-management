package dal;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import model.SystemUser;

/**
 * Implementation of DoctorDAl interface for MySql
 * @author Ras Fincher and David Jarrett
 *
 */
public class MySqlDoctorDAL implements DoctorDAL {

	@Override
	public Map<String, SystemUser> getDoctors() {
		String query = "SELECT * FROM `SystemUser` WHERE `roleID`=2;";
		Map<String, SystemUser> doctors = new HashMap<>();
		
		try (var con = DbConnection.getConnection()) {
			Statement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				int userID = rs.getInt("userID");
				String firstName = rs.getString("fname");
				String lastName = rs.getString("lname");
				LocalDate bDate = rs.getDate("bdate").toLocalDate();
				String phone = rs.getString("phone");
				SystemUser doctor = new SystemUser(Integer.toString(userID), firstName,
						lastName, bDate, phone);
				doctors.put(doctor.getFullName(), doctor);
			}
			return doctors;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return doctors;
	}

}
