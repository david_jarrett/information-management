drop table if exists Address;
create table Address (
    addressID int auto_increment,
    street varchar(30) not null,
    city varchar(20) not null,
    state char(2) not null,
    zip varchar(10) not null,
    primary key (addressID),
    foreign key fk_addressID(addressID) references Client(clientID)
    on update cascade
    on delete cascade
    );
    
drop table if exists `Client`;
create table `Client` (
    clientID int auto_increment,
    fname varchar(30) not null,
    lname varchar(30) not null,
    bdate datetime not null,
    gender varchar(6) not null,
    ssn char(9) not null,
    phone varchar(20) not null,
    primary key(clientID),
    unique(ssn)
    );

drop table if exists SystemUser;
create table SystemUser (
    `userId` int auto_increment,
    `fname` varchar(30) not null,
    `lname` varchar(30) not null,
    primary key(`userID`)
    );

drop table if exists Login;
create table Login (
    `username` varchar(15) not null,
    `password` varchar(20) not null,
    `loginID` int not null,
    primary key(`username`, `password`),
    foreign key fk_loginID(`loginID`) references SystemUser(userID)
    on update cascade
    on delete cascade
    );
